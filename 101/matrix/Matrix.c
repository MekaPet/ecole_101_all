/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   Matrix.c                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: bjuarez <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/05/14 14:59:36 by bjuarez      #+#   ##    ##    #+#       */
/*   Updated: 2019/05/14 15:23:16 by bjuarez     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft/includes/ft_int.h"
#include "libft/includes/ft_mem.h"
#include "libft/includes/ft_printf.h"
#include "libft/includes/ft_str.h"
#include "libft/includes/ft_unix.h"
#include "libft/includes/get_next_line.h"

int main(void)
{
	unsigned long i;

	i = 0;
	while (i != 18446744073709551615)
		ft_printf("{B.T.green.}%d{eoc}", i++);
	return (0);
}
