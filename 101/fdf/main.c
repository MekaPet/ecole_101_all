/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   main.c                                           .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rlegendr <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/08 09:36:54 by rlegendr     #+#   ##    ##    #+#       */
/*   Updated: 2018/11/13 14:32:01 by bjuarez     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fdf.h"

int		main(int ac, char **av)
{
	t_fdf_params	*data;
	int				i;

	if ((data = malloc(sizeof(t_fdf_params))) == NULL)
		return (0);
	data = ft_init_data(data);
	i = -1;
	if (check_error_fdf(ac, av[1], &(data->fdf)) == 0)
		return (0);
	ft_print_triple_tab(data->fdf);
	data->mlx = mlx_init();
	data->win = mlx_new_window(data->mlx, LONGX, LONGY, "FdF");
	ft_front_string(data->mlx, data->win);
	mlx_key_hook(data->win, ft_keyboard, (void*)data);
	mlx_loop(data->mlx);
	return (0);
}
