/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   custom.c                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: bjuarez <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/09 10:39:28 by bjuarez      #+#   ##    ##    #+#       */
/*   Updated: 2018/11/13 14:12:11 by bjuarez     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fdf.h"

void	ft_color_diadhb(void *str_image)
{
	int x;
	int y;
	int i;
	int j;

	y = -1;
	i = LONGX / 12 * 11;
	x = LONGX;
	while (++y < LONGY / 10 && x != i)
	{
		j = LONGX;
		while (--j > x)
			ft_fill_pixel(str_image, j, y, 0xE98000);
		x--;
	}
	y = LONGY;
	i = LONGX / 12 * 11;
	x = LONGX;
	while (--y > LONGY / 10 * 9 && x != i)
	{
		j = LONGX;
		while (--j >= x)
			ft_fill_pixel(str_image, j, y, 0xE98000);
		x--;
	}
}

void	ft_color_diaghb(void *str_image)
{
	int	x;
	int	y;
	int	i;
	int	j;

	y = -1;
	i = LONGX / 12;
	x = 0;
	while (++y < LONGY / 10 && x != i)
	{
		j = -1;
		while (++j < x)
			ft_fill_pixel(str_image, j, y, 0xE98000);
		x++;
	}
	y = LONGY;
	i = LONGX / 12;
	x = 0;
	while (--y > LONGY / 10 * 9 && x != -1)
	{
		j = -1;
		while (++j <= x)
			ft_fill_pixel(str_image, j, y, 0xE98000);
		x++;
	}
}

void	ft_tricolor_back(void *str_image)
{
	int	y;
	int	x;
	int	i;
	int	j;

	y = LONGY;
	i = LONGX / 2;
	j = 0;
	while (--y > LONGY / 10 * 9)
	{
		x = -1;
		j++;
		while (++x != i - j - (LONGX / 3))
		{
			ft_fill_pixel(str_image, i + x, y, 0xE89000);
			ft_fill_pixel(str_image, i - x, y, 0xE89000);
		}
	}
}

void	ft_tricolor(void *str_image)
{
	int	y;
	int	x;
	int	i;

	y = -1;
	i = LONGX / 2;
	while (++y < LONGY / 10)
	{
		x = -1;
		while (++x != i - y - (LONGX / 3))
		{
			ft_fill_pixel(str_image, i - x, y, 0xE89000);
			ft_fill_pixel(str_image, i + x, y, 0xE89000);
		}
	}
}

void	ft_color_back(void *str_image)
{
	int	y;
	int	x;

	y = -1;
	while (++y < LONGY / 10)
	{
		x = -1;
		while (++x < LONGX)
			ft_fill_pixel(str_image, x, y, 0xBF0000);
	}
	y = LONGY;
	while (--y > (LONGY / 10 * 9))
	{
		x = -1;
		while (++x < LONGX)
			ft_fill_pixel(str_image, x, y, 0xBF0000);
	}
}
