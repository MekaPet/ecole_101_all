/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   fdf.h                                            .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: bjuarez <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/09 09:36:57 by bjuarez      #+#   ##    ##    #+#       */
/*   Updated: 2018/11/13 14:38:10 by bjuarez     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include "minilibx_macos/mlx.h"
# include "libft/libft.h"
# define LONGX 1920
# define LONGY 1080

typedef struct	s_fdf_params
{
	char	***fdf;
	void	*mlx;
	void	*win;
	void	*str_image;
	int		up;
	int		s_x;
	int		s_y;
	int		pos_x;
	int		pos_y;
	int		b;
	int		d;

}				t_fdf_params;

typedef struct	s_bre_params
{
	int	x0;
	int	y0;
	int	x1;
	int	y1;
	int	save_x;
	int	save_y;
}				t_bre_params;

typedef struct	s_bre
{
	int dx;
	int dy;
	int sx;
	int sy;
	int err;
	int e2;
}				t_bre;

/*
** check_error.c
*/

int				error(int fd, int i);
int				check_error_fdf(int ac, char *file, char ****fdf);

/*
** read_tab.c
*/

void			ft_print_tab_line(char **tab);
void			ft_print_triple_tab(char ***tab);
int				ft_list_len(t_list *line_pixel);
char			***read_fdf(const int fd, char ***fdf);

/*
** custom.c
*/

void			ft_tricolor_back(void *str_image);
void			ft_tricolor(void *str_image);
void			ft_color_back(void *str_image);
void			ft_color_diaghb(void *str_image);
void			ft_color_diadhb(void *str_image);

/*
** image.c
*/

void			ft_rgb(int color, int *red, int *green, int *blue);
void			*ft_fill_pixel(void *str_image, int x, int y, int color);

/*
** string.c
*/

void			ft_back_string(void *mlx, void *win);
void			ft_front_string(void *mlx, void *win);

/*
** keyboard.c
*/

t_fdf_params	*ft_init_data(t_fdf_params *data);
t_fdf_params	*ft_move(t_fdf_params *data, int key);
int				ft_keyboard(int key, void *params);
void			ft_color(void *mlx, void *win);
void			custom(void *str_image);

/*
** tab_on_screen.c
*/

void			ft_print_tab_on_screen(t_fdf_params *da);
void			ft_print2(t_fdf_params *da, t_bre_params *par, int i, int j);

/*
** bresenham.c
*/

t_bre_params	*ft_bre_params(int x0, int y0, int x1, int y1);
t_bre			*ft_bre_init(t_bre *bre, t_bre_params *par);
void			bresenham(void *mlx, void *win, t_bre_params *par);

#endif
