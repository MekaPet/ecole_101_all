/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   string.c                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: bjuarez <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/13 14:18:19 by bjuarez      #+#   ##    ##    #+#       */
/*   Updated: 2018/11/13 14:48:02 by bjuarez     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fdf.h"

void	ft_back_string(void *mlx, void *win)
{
	mlx_string_put(mlx, win, LONGX / 200 * 105,
			LONGY / 50 * 2, 0x00000000, "FDF");
	mlx_string_put(mlx, win, LONGX / 50,
			LONGY / 50 * 44, 0x00FFFFFF, "Options :");
	mlx_string_put(mlx, win, LONGX / 50 * 5,
			LONGY / 50 * 44, 0x00FFFFFF, "W-A-S-D : Move");
	mlx_string_put(mlx, win, LONGX / 50 * 15,
			LONGY / 50 * 44, 0x00FFFFFF, "up / down : Contrast");
	mlx_string_put(mlx, win, LONGX / 50 * 25,
			LONGY / 50 * 44, 0x00FFFFFF, "+ /	- : Zoom");
	mlx_string_put(mlx, win, LONGX / 50 * 40,
			LONGY / 50 * 44, 0x00FFFFFF, "R : Reset");
	mlx_string_put(mlx, win, LONGX / 50 * 45,
			LONGY / 50 * 44, 0x00FFFFFF, "ESC : Quit");
}

void	ft_front_string(void *mlx, void *win)
{
	mlx_string_put(mlx, win, LONGX / 100 * 46,
			LONGY / 2, 0x00FFFFFF, "Press any key");
}
