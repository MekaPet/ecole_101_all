/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   keyboard.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: bjuarez <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/13 13:24:54 by bjuarez      #+#   ##    ##    #+#       */
/*   Updated: 2018/11/13 14:37:54 by bjuarez     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fdf.h"

t_fdf_params	*ft_init_data(t_fdf_params *data)
{
	data->up = 2;
	data->pos_x = 45;
	data->pos_y = 25;
	data->d = 30;
	data->b = 20;
	data->str_image = NULL;
	return (data);
}

t_fdf_params	*ft_move(t_fdf_params *data, int key)
{
	if (key == 24 || key == 69)
	{
		data->pos_y -= 3;
		data->d += 3;
		data->b += 2;
	}
	if (key == 27 || key == 78)
	{
		data->pos_y += 3;
		data->d -= 3;
		data->b -= 2;
	}
	if (key == 126)
		data->up += 1;
	if (key == 125)
		data->up -= 1;
	if (key == 13)
		data->pos_y -= 3;
	if (key == 0)
		data->pos_x -= 3;
	if (key == 1)
		data->pos_y += 3;
	if (key == 2)
		data->pos_x += 3;
	return (data);
}

int				ft_keyboard(int key, void *param)
{
	t_fdf_params *data;

	data = (t_fdf_params*)param;
	ft_color(data->mlx, data->win);
	if (key == 53)
		exit(0);
	data = ft_move(data, key);
	if (key == 15)
		data = ft_init_data(data);
	ft_color(data->mlx, data->win);
	ft_print_tab_on_screen(data);
	ft_back_string(data->mlx, data->win);
	return (key);
}

void			ft_color(void *mlx, void *win)
{
	void		*image;
	int			*str_image;
	int			bpp;
	int			s_l;
	int			endian;

	bpp = 32;
	s_l = LONGX * 4;
	endian = 1;
	if ((str_image = (void*)malloc(LONGX * LONGY * 4)) == NULL)
		exit(0);
	image = mlx_new_image(mlx, LONGX, LONGY);
	str_image = (int*)mlx_get_data_addr(image, &bpp, &s_l, &endian);
	custom(str_image);
	mlx_put_image_to_window(mlx, win, image, 0, 0);
}

void			custom(void *str_image)
{
	ft_color_back(str_image);
	ft_tricolor(str_image);
	ft_tricolor_back(str_image);
	ft_color_diaghb(str_image);
	ft_color_diadhb(str_image);
}
