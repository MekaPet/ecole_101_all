/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   tab_on_screen.c                                  .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: bjuarez <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/13 14:06:04 by bjuarez      #+#   ##    ##    #+#       */
/*   Updated: 2018/11/13 14:07:36 by bjuarez     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fdf.h"

void	ft_print2(t_fdf_params *da, t_bre_params *par, int i, int j)
{
	while (da->fdf[++i])
	{
		j = -1;
		while (da->fdf[i][++j])
		{
			par->x0 = da->s_x - i * da->d + j * da->d;
			par->y0 = da->s_y + i * da->b + j * da->b
				- ft_atoi(da->fdf[i][j]) * da->up;
			if (da->fdf[i][j + 1] != NULL)
			{
				par->x1 = da->s_x - i * da->d + (j + 1) * da->d;
				par->y1 = da->s_y + i * da->b + (j + 1) * da->b
					- ft_atoi(da->fdf[i][j + 1]) * da->up;
				bresenham(da->mlx, da->win, par);
			}
			if (da->fdf[i + 1] != NULL)
			{
				par->x1 = da->s_x - i * da->d + (j - 1) * da->d;
				par->y1 = da->s_y + (i + 1) * da->b + j * da->b
					- ft_atoi(da->fdf[i + 1][j]) * da->up;
				bresenham(da->mlx, da->win, par);
			}
		}
	}
}

void	ft_print_tab_on_screen(t_fdf_params *da)
{
	int				i;
	int				j;
	t_bre_params	*par;

	da->s_x = LONGX / 100 * da->pos_x;
	da->s_y = LONGY / 100 * da->pos_y;
	if ((par = malloc(sizeof(t_bre_params))) == NULL)
		exit(0);
	i = -1;
	j = -1;
	ft_print2(da, par, i, j);
}
