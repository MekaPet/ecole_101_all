/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   bresenham.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: bjuarez <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/09 15:59:49 by bjuarez      #+#   ##    ##    #+#       */
/*   Updated: 2018/11/13 13:15:35 by bjuarez     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fdf.h"

t_bre_params	*ft_bre_params(int x0, int y0, int x1, int y1)
{
	t_bre_params	*bre_params;

	if ((bre_params = malloc(sizeof(t_bre_params))) == NULL)
		exit(0);
	bre_params->x0 = x0;
	bre_params->y0 = y0;
	bre_params->x1 = x1;
	bre_params->y1 = y1;
	return (bre_params);
}

t_bre			*ft_bre_init(t_bre *bre, t_bre_params *par)
{
	bre->dx = ft_abs(par->x1 - par->x0);
	bre->dy = ft_abs(par->y1 - par->y0);
	bre->sx = par->x0 < par->x1 ? 1 : -1;
	bre->sy = par->y0 < par->y1 ? 1 : -1;
	bre->err = (bre->dx > bre->dy ? bre->dx : -(bre->dy)) / 2;
	return (bre);
}

void			bresenham(void *mlx, void *win, t_bre_params *par)
{
	t_bre			*bre;

	par->save_x = par->x0;
	par->save_y = par->y0;
	if ((bre = malloc(sizeof(t_bre))) == NULL)
		exit(0);
	bre = ft_bre_init(bre, par);
	while (1)
	{
		mlx_pixel_put(mlx, win, par->save_x, par->save_y, 0x00FFFFFF);
		if (par->save_x == par->x1 && par->save_y == par->y1)
			break ;
		bre->e2 = bre->err;
		if (bre->e2 > -bre->dx)
		{
			bre->err -= bre->dy;
			par->save_x += bre->sx;
		}
		if (bre->e2 < bre->dy)
		{
			bre->err += bre->dx;
			par->save_y += bre->sy;
		}
	}
}
