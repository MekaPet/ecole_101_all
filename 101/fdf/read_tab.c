/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   read_tab.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: bjuarez <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/09 13:31:20 by bjuarez      #+#   ##    ##    #+#       */
/*   Updated: 2018/11/09 13:31:54 by bjuarez     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fdf.h"

void	ft_print_tab_line(char **tab)
{
	int		j;

	j = -1;
	while (tab[++j])
	{
		if (tab[j + 1] == NULL)
			ft_putstr(tab[j]);
		else
		{
			ft_putstr(tab[j]);
			ft_putchar(' ');
		}
	}
}

void	ft_print_triple_tab(char ***tab)
{
	int		i;

	i = -1;
	while (tab[++i])
	{
		ft_print_tab_line(tab[i]);
		ft_putchar('\n');
	}
}

int		ft_list_len(t_list *line_pixel)
{
	t_list	*temp;
	int		i;

	temp = line_pixel;
	i = 0;
	while (temp != NULL)
	{
		temp = temp->next;
		i++;
	}
	return (i);
}

char	***read_fdf(const int fd, char ***fdf)
{
	t_list	*list_line;
	char	*line;
	int		i;
	int		len_list;

	i = -1;
	while (get_next_line(fd, &line) == 1)
		ft_lstbck(&list_line, line, ft_strlen(line) + 1);
	len_list = ft_list_len(list_line);
	if (!(fdf = (char***)malloc(sizeof(char**) * len_list + 1)))
		return (NULL);
	while (++i < len_list)
	{
		fdf[i] = ft_strsplit(list_line->content, ' ');
		list_line = list_line->next;
	}
	fdf[i] = NULL;
	return (fdf);
}
