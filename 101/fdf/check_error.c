/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   check_error.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: bjuarez <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/13 13:16:51 by bjuarez      #+#   ##    ##    #+#       */
/*   Updated: 2018/11/13 13:17:19 by bjuarez     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fdf.h"

int		error(int fd, int i)
{
	if (i == 0)
	{
		ft_putstr("usage: ./fdf targetfile\n");
		return (0);
	}
	if (i == 1)
	{
		close(fd);
		ft_putstr("error\n");
		return (0);
	}
	if (i == 2)
	{
		close(fd);
		ft_putstr("error map\n");
		return (0);
	}
	return (0);
}

int		check_error_fdf(int ac, char *file, char ****fdf)
{
	int	fd;

	fd = 0;
	if (ac != 2)
		return (error(fd, 0));
	if ((fd = open(file, O_RDONLY)) < 0)
		return (error(fd, 1));
	if ((*fdf = read_fdf(fd, *fdf)) == NULL)
		return (error(fd, 2));
	close(fd);
	return (1);
}
