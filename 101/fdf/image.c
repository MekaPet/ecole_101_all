/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   image.c                                          .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: bjuarez <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/13 13:58:19 by bjuarez      #+#   ##    ##    #+#       */
/*   Updated: 2018/11/13 14:55:25 by bjuarez     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fdf.h"

void	ft_rgb(int color, int *blue, int *green, int *red)
{
	if (color == 0xE98000)
	{
		*blue = 0;
		*green = 128;
		*red = 223;
	}
	if (color == 0xBF0000)
	{
		*blue = 0;
		*green = 0;
		*red = 191;
	}
}

void	*ft_fill_pixel(void *str_image, int x, int y, int color)
{
	int		i;
	int		red;
	int		green;
	int		blue;

	ft_rgb(color, &blue, &green, &red);
	i = y * LONGX * 4 + x * 4;
	((char*)str_image)[i] = (char)blue;
	((char*)str_image)[i + 1] = (char)green;
	((char*)str_image)[i + 2] = (char)red;
	((char*)str_image)[i + 3] = '0';
	return (str_image);
}
