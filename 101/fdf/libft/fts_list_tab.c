/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   list_tab.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rlegendr <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/26 09:31:47 by rlegendr     #+#   ##    ##    #+#       */
/*   Updated: 2018/11/08 15:11:50 by rlegendr    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstbck(t_list **cr, char *content, size_t content_size)
{
	t_list	*newend;
	t_list	*temp;

	newend = ft_lstnew(content, content_size);
	if (*cr == NULL)
		*cr = newend;
	else
	{
		temp = *cr;
		while (temp->next != NULL)
			temp = temp->next;
		temp->next = newend;
	}
}

void	ft_lst_totaldel(t_list **alst)
{
	t_list	*data;
	t_list	*lst;

	if (!alst)
		return ;
	data = *alst;
	while (data)
	{
		lst = data->next;
		free(data->content);
		data->content_size = 0;
		free(data);
		data = lst;
	}
	*alst = NULL;
}

void	ft_tabdel(char **tab)
{
	int i;
	int j;

	i = -1;
	while (tab[++i])
	{
		j = -1;
		while (tab[i][++j])
			tab[i][j] = '\0';
	}
	i = -1;
	while (tab[++i])
		free(tab[i]);
	tab = NULL;
}

void	ft_print_tab(char **tab)
{
	int i;

	i = -1;
	while (tab[++i])
	{
		ft_putstr(tab[i]);
		ft_putchar('\n');
	}
}
