#include <unistd.h>
#include <stdlib.h>

void	brainfuck(char *input)
{
	unsigned char stack[2048];
	unsigned char *ptr = stack;
	size_t i;
	size_t loop;

	i = 0;
	while (i < 2048)
		stack[i++] = 0;
	i = 0;
	while (input[i])
	{
		if (input[i] == '>')
			++ptr;
		else if (input[i] == '<')
			--ptr;
		else if (input[i] == '+')
			++*ptr;
		else if (input[i] == '-')
			--*ptr;
		else if (input[i] == '.')
			write(1, ptr, 1);
		else if (input[i] == '[' && *ptr == 0)
		{
			i++;
			loop = 0;
			while (input[i])
			{
				if (input[i] == '[')
					loop++;
				else if (input[i] == ']' && loop == 0)
					break ;
				if (input[i] == ']')
					loop--;
				i++;
			}
		}
		else if (input[i] == ']' && *ptr)
		{
			i--;
			loop = 0;
			while (input[i])
			{
				if (input[i] == ']')
					loop++;
				else if (input[i] == '[' && loop == 0)
					break ;
				if (input[i] == '[')
					loop--;
				i--;
			}
		}
		i++;
	}
}

int		main(int ac, char **av)
{
	if (ac == 2)
		brainfuck(av[1]);
	else
		write(1, "\n", 1);
	return (0);
}
