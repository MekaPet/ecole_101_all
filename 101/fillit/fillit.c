/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   fillit.c                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rlegendr <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/25 11:43:29 by rlegendr     #+#   ##    ##    #+#       */
/*   Updated: 2018/11/09 15:07:11 by rlegendr    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fillit.h"

char	**ft_fill_form(char **tab_f, char c, char *path, int pos)
{
	int		x;
	int		y;

	y = pos / ft_strlen(tab_f[0]);
	x = pos % ft_strlen(tab_f[0]);
	pos = -1;
	tab_f[y][x] = c;
	while (path[++pos])
	{
		if (x != (int)ft_strlen(tab_f[0]) && path[pos] == 'd' &&
				(tab_f[y][x + 1] == c || tab_f[y][x + 1] == '.'))
			tab_f[y][++x] = c;
		else if (path[pos] == 'g' &&
				(tab_f[y][x - 1] == c || tab_f[y][x - 1] == '.'))
			tab_f[y][--x] = c;
		else if (tab_f[y + 1] != NULL && path[pos] == 'b' &&
				(tab_f[y + 1][x] == c || tab_f[y + 1][x] == '.'))
			tab_f[++y][x] = c;
		else
		{
			tab_f = ft_clear_letter(tab_f, c);
			return (NULL);
		}
	}
	return (tab_f);
}

int		ft_big_square(char ***temp, char ***tab_fillit, int i)
{
	int		pos;

	*temp = ft_malloc_tab(i);
	ft_tabdel(*tab_fillit);
	*tab_fillit = *temp;
	if ((pos = ft_find_letter(*tab_fillit, '.', i, 1)) < 0)
		return (0);
	return (pos);
}

int		ft_move(char **tab_f, int pos, int i, char c)
{
	pos = ft_find_letter(tab_f, c, i, 1) + 1;
	ft_clear_letter(tab_f, c);
	pos = ft_pos(pos, i, tab_f);
	return (pos);
}

int		ft_pos(int pos, int i, char **tab_f)
{
	while (!((pos + 1) / i == i && (pos + 1) % i == 0) &&
			tab_f[pos / i][pos % i] != '.')
		pos++;
	return (pos);
}

char	**fillit(char **tab, char **tab_f, char **temp, char c)
{
	int		pos;
	int		i;

	i = ft_strlen(tab_f[0]);
	pos = 0;
	while (c - 65 < ft_count_block(tab))
	{
		if ((temp = ft_fill_form(tab_f, c, ft_path(tab, c), pos++)) == NULL)
		{
			pos = ft_pos(pos, i, tab_f);
			if ((pos + 1) / i == i && (pos + 1) % i == 0 && c > 'A')
				pos = ft_move(tab_f, pos, i, --c);
			if ((pos + 1) / i == i && (pos + 1) % i == 0 && c == 'A')
				pos = ft_big_square(&temp, &tab_f, ++i);
		}
		else
		{
			if (c++ >= 0 && (pos = ft_find_letter(tab_f, '.', i, 1)) < 0)
				pos = 0;
			if (pos + 1 == i * i)
				pos--;
		}
	}
	return (tab_f);
}
