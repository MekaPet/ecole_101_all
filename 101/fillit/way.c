/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   way.c                                            .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rlegendr <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/29 08:07:26 by rlegendr     #+#   ##    ##    #+#       */
/*   Updated: 2018/11/07 14:49:13 by rlegendr    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fillit.h"

int		ft_find_letter(char **tab, char c, int i, int n)
{
	int		verif;
	int		x;
	int		y;

	x = 0;
	y = 0;
	verif = -1;
	while (tab[y] && n >= 1 && verif == -1)
	{
		x = 0;
		while (tab[y][x] && verif == -1)
		{
			if (tab[y][x] == c && n == 1)
				verif = 1;
			if (tab[y][x] == c && n > 1)
				n--;
			x++;
		}
		y++;
	}
	if (x == 0 && y == 0)
		return (0);
	return (verif * (i * (y - 1) + (x - 1)));
}

char	*ft_path(char **tab, char c)
{
	int		y;
	int		x;
	char	*result;
	int		temp;
	int		i;

	i = ft_strlen(tab[0]);
	if ((temp = ft_find_letter(tab, c, i, 1)) < 0)
		return (NULL);
	y = temp / 4;
	x = temp % 4;
	result = ft_result(tab, x, y, c);
	return (result);
}

char	*ft_result(char **tab, int x, int y, char c)
{
	int		down;
	int		temp;
	char	*result;

	if (!(result = (char*)malloc(10)))
		return (NULL);
	temp = 0;
	down = 1;
	while (tab[y + 1][x] == c || tab[y][x + 1] == c || down != 0)
	{
		while (tab[y][x + 1] == c && x++ >= 0)
			result[temp++] = 'd';
		while (tab[y][x - 1] == c && x-- >= 0)
			result[temp++] = 'g';
		while (tab[y + 1][x] != c && tab[y][x + 1] == c && x++ >= 0)
			result[temp++] = 'd';
		down = 0;
		if (tab[y + 1][x] == c && y++ >= 0)
		{
			result[temp++] = 'b';
			down = 1;
		}
	}
	result[temp] = '\0';
	return (result);
}
