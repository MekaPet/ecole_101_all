/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   check_tetriminos.c                               .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rlegendr <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/24 16:03:11 by rlegendr     #+#   ##    ##    #+#       */
/*   Updated: 2018/11/09 15:25:28 by rlegendr    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fillit.h"

char	**ft_change_tab(char **tab)
{
	int		i;
	int		j;
	char	block;

	i = -1;
	block = 'A';
	while (tab[++i])
	{
		j = -1;
		if ((i + 1) % 5 == 0)
			block++;
		while (tab[i][++j])
		{
			if (tab[i][j] == '#')
				tab[i][j] = block;
		}
	}
	return (tab);
}

int		ft_count_diese(char **tab)
{
	int i;
	int j;
	int diese;

	i = -1;
	diese = 0;
	while (tab[++i])
	{
		j = -1;
		while (tab[i][++j])
		{
			if (tab[i][j] == '#')
				diese++;
		}
	}
	return (diese);
}

int		ft_check_tab(char **tab)
{
	int i;
	int j;
	int diese;

	i = -1;
	diese = 0;
	while (tab[++i])
	{
		j = -1;
		while (tab[i][++j])
		{
			if (tab[i][j] != '.' && tab[i][j] != '#' &&
					ft_strcmp(tab[i], "    ") != 0)
				return (0);
			if (tab[i][j] == '#')
				diese++;
			if ((i + 1) % 5 != 0 && ft_strcmp(tab[i], "    ") == 0)
				return (0);
		}
	}
	if (i > 130)
		return (0);
	if (i - i / 5 != diese)
		return (0);
	return (1);
}

int		ft_links(char **tab, int i, int j, int link)
{
	if (tab[i][j + 1] == '#' && tab[i][j] == '#')
		link++;
	if (tab[i + 1][j] == '#' && tab[i][j] == '#')
		link++;
	if (tab[i + 1][j + 1] == '#' && tab[i + 1][j] == '#' &&
			tab[i][j + 1] == '#' && tab[i][j] == '#')
		link--;
	return (link);
}

int		ft_check_form(char **tab)
{
	int i;
	int j;
	int link;

	i = 0;
	link = 0;
	while (tab[i])
	{
		if ((i + 1) % 5 == 0)
			link = 0;
		j = 0;
		while (tab[i][j])
		{
			if (ft_strcmp(tab[i], "    ") != 0)
				link = ft_links(tab, i, j, link);
			j++;
		}
		i++;
		if (link != 3 && (i + 1) % 5 == 0)
			return (0);
	}
	return (1);
}
