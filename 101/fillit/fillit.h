/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   fillit.h                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rlegendr <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/24 09:17:31 by rlegendr     #+#   ##    ##    #+#       */
/*   Updated: 2018/11/09 15:30:36 by rlegendr    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H

# include "libft/libft.h"

/*
** main.c
*/

char	**ft_check_read(const int fd, char **tab);
char	**ft_fill_tab(t_list *cr, int i);
int		error(int fd, int i);
int		check_error(int argc, char *argv, char ***tab);

/*
** check_tetriminos.c
*/

char	**ft_change_tab(char **tab);
int		ft_count_diese(char **tab);
int		ft_check_tab(char **tab);
int		ft_links(char **tab, int i, int j, int link);
int		ft_check_form(char **tab);

/*
** list_tab.c
*/

void	ft_print_tab(char **tab);
void	ft_tabdel(char **tab);
void	ft_lst_totaldel(t_list **alst);
void	ft_lstbck(t_list **cr, char *content, size_t content_size);

/*
** fillit.c
*/

int		ft_move(char **tab_f, int pos, int i, char c);
int		ft_pos(int pos, int i, char **tab_f);
int		ft_big_square(char ***temp, char ***tab_fillit, int i);
char	**fillit(char **tab, char **tab_fillit, char **temp, char c);
char	**ft_fill_form(char **tab_f, char c, char *path, int pos);

/*
** op_fillit.c
*/

char	**ft_clear_letter(char **tab, char c);
int		ft_count_block(char **tab);
char	**ft_malloc_tab(int len);

/*
** way.c
*/

int		ft_find_letter(char **tab, char c, int i, int n);
char	*ft_path(char **tab, char c);
char	*ft_result(char **tab, int x, int y, char c);

#endif
