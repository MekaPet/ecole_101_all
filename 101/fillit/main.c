/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   main.c                                           .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rlegendr <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/24 09:04:38 by rlegendr     #+#   ##    ##    #+#       */
/*   Updated: 2018/11/09 15:25:09 by rlegendr    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fillit.h"

char	**ft_fill_tab(t_list *cr, int i)
{
	char	**tab;
	int		j;

	j = 0;
	if (!(tab = ft_memalloc(i + 1)))
		return (NULL);
	while (cr)
	{
		if (!(tab[j] = ft_memalloc(4 + 1)))
			return (NULL);
		tab[j] = ft_strcpy(tab[j], cr->content);
		cr = cr->next;
		j++;
	}
	if (!(tab[j] = ft_memalloc(4 + 1)))
		return (NULL);
	tab[j] = ft_strcpy(tab[j], "    ");
	tab[j + 1] = NULL;
	return (tab);
}

char	**ft_check_read(const int fd, char **tab)
{
	t_list	*cr;
	char	*line;
	int		i;
	int		j;

	i = 1;
	line = NULL;
	cr = NULL;
	while (get_next_line(fd, &line) == 1)
	{
		j = ft_strlen(line);
		if ((i % 5 == 0 || j != 4) && *line != '\0')
			return (NULL);
		*line != '\0' ? ft_lstbck(&cr, line, j) : ft_lstbck(&cr, "    ", 4);
		i++;
	}
	if ((i - (i / 5)) % 4 != 0)
	{
		ft_lst_totaldel(&cr);
		return (NULL);
	}
	tab = ft_fill_tab(cr, i);
	ft_lst_totaldel(&cr);
	return (tab);
}

int		error(int fd, int i)
{
	if (i == 0)
	{
		ft_putstr("usage: ./fillit targetfile\n");
		return (0);
	}
	if (i == 1)
	{
		close(fd);
		ft_putstr("error\n");
		return (0);
	}
	return (0);
}

int		check_error(int argc, char *argv, char ***tab)
{
	int		fd;

	fd = 0;
	if (argc != 2)
		return (error(fd, 0));
	if ((fd = open(argv, O_RDONLY)) < 0)
		return (error(fd, 1));
	if ((*tab = ft_check_read(fd, *tab)) == NULL)
		return (error(fd, 1));
	if (ft_check_tab(*tab) == 0)
		return (error(fd, 1));
	if (ft_check_form(*tab) == 0)
		return (error(fd, 1));
	if ((*tab = ft_change_tab(*tab)) == NULL)
		return (error(fd, 1));
	close(fd);
	return (1);
}

int		main(int argc, char **argv)
{
	char	**tab;
	char	**tab_fillit;
	char	**temp;
	char	c;

	tab_fillit = NULL;
	tab_fillit = ft_malloc_tab(2);
	temp = NULL;
	c = 'A';
	if (check_error(argc, argv[1], &tab) == 0)
		return (0);
	tab_fillit = fillit(tab, tab_fillit, temp, c);
	ft_print_tab(tab_fillit);
	return (0);
}
