/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   fillit_input.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rlegendr <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/06 09:43:32 by rlegendr     #+#   ##    ##    #+#       */
/*   Updated: 2018/11/08 08:25:56 by rlegendr    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fillit.h"

int		ft_count_block(char **tab)
{
	int i;
	int j;
	int cpt;

	i = -1;
	cpt = 0;
	while (tab[++i])
	{
		j = -1;
		while (tab[i][++j])
		{
			if (tab[i][j] >= 'A' && tab[i][j] <= 'Z')
				cpt++;
		}
	}
	return (cpt / 4);
}

char	**ft_clear_letter(char **tab, char c)
{
	int		i;
	int		j;

	i = -1;
	while (tab[++i])
	{
		j = -1;
		while (tab[i][++j])
		{
			if (tab[i][j] == c)
				tab[i][j] = '.';
		}
	}
	return (tab);
}

char	**ft_malloc_tab(int len)
{
	char	**tab;
	int		i;
	int		j;

	i = -1;
	if ((tab = ft_memalloc(len + 1)) == NULL)
		return (NULL);
	while (++i < len)
	{
		j = -1;
		if ((tab[i] = ft_memalloc(len + 1)) == NULL)
			return (NULL);
		while (++j < len)
			tab[i][j] = '.';
		tab[i][j] = '\0';
	}
	tab[i] = NULL;
	return (tab);
}
