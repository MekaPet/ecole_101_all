/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   get_next_line.c                                  .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: bjuarez <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/12 13:12:32 by bjuarez      #+#   ##    ##    #+#       */
/*   Updated: 2018/10/25 11:26:24 by bjuarez     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "get_next_line.h"

char	*get_line(char *str, char **line)
{
	char		*temp;
	int			i;
	int			newl;

	i = 0;
	newl = 0;
	if (!(*str))
		return (NULL);
	while (str[i] != '\n' && str[i] != '\0')
		i++;
	if (str[i] == '\n')
		newl = 1;
	*line = ft_strsub(str, 0, i);
	temp = ft_strsub(str, i + newl, (ft_strlen(str) - (i + newl));
	str = ft_strcpy(str, temp);
	free(temp);
	return (*line);
}

int		get_next_line(const int fd, char **line)
{
	static char	*str[0];
	char		buf[BUFF_SIZE + 1];
	char		*temp;
	int			ret;

	ret = 1;
	if (fd < 0 || line == NULL || BUFF_SIZE < 1)
		return (-1);
	if (!(str[fd]))
		str[fd] = ft_strnew(1);
	while (!ft_strchr(str[fd], '\n') && ret > 0)
	{
		if ((ret = read(fd, buf, BUFF_SIZE)) > 0)
		{
			buf[ret] = '\0';
			temp = ft_strjoin(str[fd], buf);
			free(str[fd]);
			str[fd] = temp;
		}
	}
	if (ret < 0)
		return (-1);
	if ((*line = get_line(str[fd], line)) == NULL)
		return (0);
	return (1);
}
