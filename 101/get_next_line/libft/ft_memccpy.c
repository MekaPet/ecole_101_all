/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_memccpy.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: bjuarez <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/10 09:29:52 by bjuarez      #+#   ##    ##    #+#       */
/*   Updated: 2018/10/10 09:29:53 by bjuarez     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

void				*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	size_t			i;
	unsigned char	*str_d;
	unsigned char	*str_s;

	str_d = (unsigned char*)dst;
	str_s = (unsigned char*)src;
	i = 0;
	while (i < n)
	{
		str_d[i] = str_s[i];
		if (str_s[i] == ((unsigned char)c))
			return (&dst[i + 1]);
		i++;
	}
	return (NULL);
}
