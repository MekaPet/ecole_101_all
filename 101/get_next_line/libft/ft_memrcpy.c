/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_memrcpy.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: bjuarez <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/17 10:41:11 by bjuarez      #+#   ##    ##    #+#       */
/*   Updated: 2018/10/17 13:14:05 by bjuarez     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memrcpy(void *dst, const void *src, size_t n)
{
	size_t	i;
	char	*str_dst;
	char	*str_src;

	i = n;
	str_dst = (char*)dst;
	str_src = (char*)src;
	if (n == 0)
		return (dst);
	while ((int)--i >= 0)
	{
		str_dst[i] = str_src[i];
	}
	return (dst);
}
