/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strchr.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: bjuarez <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/10 09:35:22 by bjuarez      #+#   ##    ##    #+#       */
/*   Updated: 2018/10/10 09:35:22 by bjuarez     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strchr(const char *s, int c)
{
	int		i;
	char	*str_s;

	i = 0;
	str_s = (char*)s;
	while (str_s[i] != '\0')
	{
		if (str_s[i] == (char)c)
			return (&str_s[i]);
		i++;
	}
	if (c == 0 && s)
		return (&str_s[ft_strlen(s)]);
	return (NULL);
}
