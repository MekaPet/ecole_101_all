/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_nmatch.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: bjuarez <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/17 10:55:07 by bjuarez      #+#   ##    ##    #+#       */
/*   Updated: 2018/10/17 10:55:34 by bjuarez     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

int	ft_nmatch(char *s1, char *s2)
{
	if (!s1 || !s2)
		return (0);
	if (*s1 == '\0' && *s2 == '\0')
		return (1);
	else if (*s2 == '*')
	{
		if (*s1 == '\0')
			return (ft_nmatch(s1, s2 + 1));
		else
			return (ft_nmatch(s1 + 1, s2) + ft_nmatch(s1, s2 + 1));
	}
	else if (*s1 == *s2)
		return (ft_nmatch(s1 + 1, s2 + 1));
	return (0);
}
