/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_atoi.c                                        .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: bjuarez <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/10 09:25:27 by bjuarez      #+#   ##    ##    #+#       */
/*   Updated: 2018/10/11 16:15:13 by bjuarez     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

int	ft_atoi(const char *str)
{
	short int				s;
	unsigned long long int	nb;

	nb = 0;
	while ((*str >= 9 && *str <= 13) || *str == 32)
		str++;
	s = (*str == '-') ? 1 : 0;
	*str == '-' || *str == '+' ? str++ : 0;
	while (ft_isdigit(*str))
	{
		nb = nb * 10 + *str - 48;
		str++;
	}
	if (s == 1 && nb > 9223372036854775807)
		return (0);
	if (s == 0 && nb > 9223372036854775807)
		return (-1);
	return (s ? -nb : nb);
}
