/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_itoa.c                                        .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: bjuarez <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/10 09:29:16 by bjuarez      #+#   ##    ##    #+#       */
/*   Updated: 2018/10/10 09:29:18 by bjuarez     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

static int	ft_count_n(int temp)
{
	int		i;

	i = 0;
	if (temp == 0)
		return (1);
	while (temp != 0 && i++ > -1)
		temp = temp / 10;
	return (i);
}

static char	*ft_itoa2(char *str, int neg, int i, int nbr)
{
	if (nbr == -2147483648)
		return (ft_strdup("-2147483648"));
	if (neg == 1)
	{
		nbr = -nbr;
		str[0] = '-';
	}
	while (neg == 0 ? --i >= 0 : --i >= 1)
	{
		str[i] = (nbr % 10) + 48;
		nbr = nbr / 10;
	}
	return (str);
}

char		*ft_itoa(int nbr)
{
	char	*str;
	int		neg;
	int		i;

	neg = 0;
	i = ft_count_n(nbr);
	if (nbr < 0)
	{
		neg++;
		i++;
	}
	if (!(str = malloc(sizeof(char) * (i + 1))))
		return (NULL);
	str[i] = '\0';
	return (ft_itoa2(str, neg, i, nbr));
}
