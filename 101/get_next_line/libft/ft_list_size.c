/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_list_size.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: bjuarez <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/11 17:46:35 by bjuarez      #+#   ##    ##    #+#       */
/*   Updated: 2018/10/11 17:47:30 by bjuarez     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

int	ft_list_size(t_list *begin_list)
{
	t_list	*temp;
	int		i;

	i = 1;
	temp = begin_list;
	while (temp->next != NULL)
	{
		temp = temp->next;
		i++;
	}
	return (i);
}
