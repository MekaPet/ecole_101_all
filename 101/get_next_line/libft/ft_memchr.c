/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_memchr.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: bjuarez <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/10 09:30:08 by bjuarez      #+#   ##    ##    #+#       */
/*   Updated: 2018/10/10 17:20:12 by bjuarez     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

void				*ft_memchr(const void *s, int c, size_t n)
{
	unsigned char c2;
	unsigned char *str;

	if (n == 0)
		return (NULL);
	str = (unsigned char *)s;
	c2 = (unsigned char)c;
	while (n-- > 0)
	{
		if (*str == c2)
			return ((void *)str);
		str++;
	}
	return (NULL);
}
