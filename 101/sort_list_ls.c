/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   sort_list_ls.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: bjuarez <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/02/18 13:39:42 by bjuarez      #+#   ##    ##    #+#       */
/*   Updated: 2019/02/18 16:39:13 by bjuarez     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

int		ascii(t_ls *tmp, t_ls *tmp2)
{
	if (ft_strcmp(tmp->path, tmp2->path) <= 0)
		return (1);
	return (0);
}

int		min(t_ls *tmp, t_ls *tmp2, t_flags_ls *d)
{
	d->a = d->a == 0 ? 0 : 1;
	if (tmp->sec >= tmp2->sec)
	{
		if (tmp->sec == tmp2->sec)
			return (ascii(tmp, tmp2));
		return (1);
	}
	return (0);
}

int		hour(t_ls *tmp, t_ls *tmp2, t_flags_ls *d)
{
	d->a = d->a == 0 ? 0 : 1;

	if (tmp->min >= tmp2->min)
	{
		if (tmp->min == tmp2->min)
			return (min(tmp, tmp2, d));
		return (1);
	}
	return (0);
}

int		day(t_ls *tmp, t_ls *tmp2, t_flags_ls *d)
{
	if (tmp->hour >= tmp2->hour)
	{
		if (tmp->hour == tmp2->hour)
			return (hour(tmp, tmp2, d));
		return (1);
	}
	return (0);
}

int		month(t_ls *tmp, t_ls *tmp2, t_flags_ls *d)
{
	if (tmp->day >= tmp2->day)
	{
		if (tmp->day == tmp2->day)
			return (day(tmp, tmp2, d));
		return (1);
	}
	return (0);
}

int		year(t_ls *tmp, t_ls *tmp2, t_flags_ls *d)
{
	if (tmp->nb_month >= tmp2->nb_month)
	{
		if (tmp->nb_month == tmp2->nb_month)
			return (month(tmp, tmp2, d));
		return (1);
	}
	return (0);
}

int		sort_by_type(t_ls *tmp, t_ls *tmp2, t_flags_ls *d, int use)
{
	if (use == 0)
	{
//		if ((tmp->type == -1 && (tmp2->type == 1 || tmp2->type == 0)) || (tmp2->type == -1 && (tmp->type == 1 || tmp->type == 0)))
//		{
			if (tmp->type <= tmp2->type)
			{
				if (tmp->type == -1 && tmp2->type == -1)
					return (ascii(tmp, tmp2));
				return (1);
			}
//			return (1);
//		}
		return (0);
	}
	if (d->upper_s == 1)
	{
		if (tmp->size >= tmp2->size)
			return (1);
		return (0);
	}
	if (d->t == 1)// || d->u == 1)
	{
		if (tmp->day >= tmp2->day)
		{
			if (tmp->year == tmp2->year)
				return (year(tmp, tmp2, d));
			return (1);
		}
		return (0);
	}
	if (d->f == 0 && d->t == 0 && d->upper_s == 0)
	{
		if (ft_strcmp(tmp->path, tmp2->path) <= 0)
			return (1);
		return (0);
	}
	return (1);
}

int			head_go_swap(t_ls **head, t_ls **go, t_ls **swap, t_ls **swap2)
{
	if (*swap == *go && *go == *head)
	{
		(*swap)->next = (*swap2)->next;
		(*swap2)->next = *swap;
		*go = *swap2;
		*head = *swap2;
		*swap = (*swap)->next;
		*swap2 = (*swap2)->next;
	}
	else if (*swap != *go)
	{
		(*swap)->next = (*swap2)->next;
		(*swap2)->next = *swap;
		(*go)->next = *swap2;
		*go = *swap2;
		*swap = (*swap)->next;
		*swap2 = (*swap2)->next;
	}
	*go = *head;
	*swap = *go;
	if ((*swap)->next == NULL)
		return (0);
	*swap2 = (*swap)->next;
	return (1);
}

int			go_swap(t_ls **swap, t_ls **swap2)
{
	*swap = (*swap)->next;
	if ((*swap)->next == NULL)
		return (0);
	*swap2 = (*swap2)->next;
	return (1);
}

int			all_different(t_ls **go, t_ls **swap, t_ls **swap2)
{
	*go = (*go)->next;
	*swap = (*swap)->next;
	if ((*swap)->next == NULL)
		return (0);
	*swap2 = (*swap2)->next;
	return (1);
}

t_ls		*generic_sort_list(t_ls *head, t_ls *go, t_flags_ls *d, int use)
{
	t_ls *swap;
	t_ls *swap2;

	swap = go;
	swap2 = swap->next;
	while (go)
	{
		if (sort_by_type(swap, swap2, d, use) == 0)
		{
			if (head_go_swap(&head, &go, &swap, &swap2) == 0)
				break ;
		}
		else if (go == swap)
		{
			if (go_swap(&swap, &swap2) == 0)
				break ;
		}
		else
		{
			if (all_different(&go, &swap, &swap2) == 0)
				break ;
		}
	}
	return (head);
}

t_ls		*sort_list_ls(t_ls *head, t_flags_ls *d)
{
	t_ls *go;

	if (head)
		go = head;
	else
		return (head);
	d->a = d->a == 1 ? 1 : 0;
//	head = generic_sort_list(head, go, d, 0);
	head = generic_sort_list(head, go, d, 1);
	return (head);
}
