/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   display_ls.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: bjuarez <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/02/22 10:33:35 by bjuarez      #+#   ##    ##    #+#       */
/*   Updated: 2019/02/25 14:12:59 by rlegendr    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

void	color_executable_path(t_ls *ls, int name_index)
{
	if (ls->filestat.st_mode & 04000 && ls->permission[0] == '-' &&
			((ls->permission[3] == 'x' || ls->permission[6] == 'x' ||
			ls->permission[9] == 'x' || ls->permission[3] == 's' ||
			ls->permission[6] == 's' || ls->permission[9] == 't') &&
			ls->permission[0] == '-'))
		ft_printf("{S.red.T.grey.}%s{eoc}", ls->path + name_index);
	else if ((ls->filestat.st_mode & 02000 || (ls->filestat.st_mode & 01000 &&
			ls->filestat.st_mode & 02000)) && ((ls->permission[3] == 'x' ||
			ls->permission[6] == 'x' || ls->permission[9] == 'x' ||
			ls->permission[3] == 's' || ls->permission[6] == 's' ||
			ls->permission[9] == 't') && ls->permission[0] == '-'))
		ft_printf("{S.cyan.T.grey.}%s{eoc}", ls->path + name_index);
	else if ((ls->permission[3] == 'x' || ls->permission[6] == 'x' ||
			ls->permission[9] == 'x' || ls->permission[3] == 's' ||
			ls->permission[6] == 's' || ls->permission[9] == 't') &&
			ls->permission[0] == '-')
		ft_printf("{T.red.}%s{eoc}", ls->path + name_index);
	else if (ls->permission[0] == '-')
		ft_printf("%s", ls->path + name_index);
}

void	color_path_ls(t_ls *ls, t_flags_ls *d, int usage)
{
	int		name_index;

	name_index = 0;
	if (usage == 1)
		name_index = get_name_index(ls->path);
	if (d->upper_g == 0)
	{
		ft_printf("%s", ls->path + name_index);
		return ;
	}
	if (ls->permission[0] == 'd')
		ft_printf("{B.T.cyan.}%s{eoc}", ls->path + name_index);
	else if (ls->permission[0] == 'l')
		ft_printf("{T.purple.}%s{eoc}", ls->path + name_index);
	else if (ls->permission[0] == 'p')
		ft_printf("{T.yellow.}%s{eoc}", ls->path + name_index);
	else if (ls->permission[0] == 'c')
		ft_printf("{T.blue.S.yellow.}%s{eoc}", ls->path + name_index);
	else if (ls->permission[0] == 'b')
		ft_printf("{T.blue.S.cyan.}%s{eoc}", ls->path + name_index);
	else
		color_executable_path(ls, name_index);
}

void	print_nb_block(t_ls *ls, t_flags_ls *d)
{
	int		nb_char_block;

	nb_char_block = check_num_length(ls->nb_block);
	while (nb_char_block++ < d->nb_char_block)
		ft_putchar(' ');
	ft_printf("{B.}%ld{eoc} ", ls->nb_block);
}

void	display_ls(t_ls *ls, t_flags_ls *d, int usage)
{
	if (verif_ls_path(ls->path) == 0 && d->a == 0)
		return ;
	if (d->l == 1)
	{
		if (d->s == 1)
			print_nb_block(ls, d);
		ft_printf("%s ", ls->permission);
		print_nb_link_ls(ls, d);
		if (d->g == 0)
			print_uid_ls(ls, d);
		if (d->o == 0)
			print_gid_ls(ls, d);
		if (ls->permission[0] != 'c' && ls->permission[0] != 'b')
			print_size_ls(ls, d);
		else
			print_dev_ls(ls, d);
		print_time_ls(ls, d);
	}
	color_path_ls(ls, d, usage);
	if (ls->permission[0] == 'l' && d->l == 1)
		print_link_ls(ls);
	ft_putchar('\n');
}

void	print_ls(t_ls *ls, t_flags_ls *d, int usage)
{
	if (d->r == 0)
	{
		while (ls != NULL)
		{
			display_ls(ls, d, usage);
			ls = ls->next;
		}
	}
	else
	{
		while (ls->next != NULL)
			ls = ls->next;
		while (ls != NULL)
		{
			display_ls(ls, d, usage);
			ls = ls->before;
		}
	}
}
