/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   print_args_time_ls.c                             .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: bjuarez <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/02/22 10:28:09 by bjuarez      #+#   ##    ##    #+#       */
/*   Updated: 2019/02/25 10:21:00 by rlegendr    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

void	print_dev_ls(t_ls *ls, t_flags_ls *d)
{
	int		i;
	int		nb_maj;
	int		nb_min;

	nb_maj = check_num_length(ls->dev_maj);
	nb_min = check_num_length(ls->dev_min);
	i = nb_maj;
	while (i++ < d->nb_char_maj)
		ft_putchar(' ');
	ft_printf("{B.}%d{eoc}, ", ls->dev_maj);
	i = nb_min;
	while (i++ < d->nb_char_min)
		ft_putchar(' ');
	ft_printf("{B.}%d{eoc} ", ls->dev_min);
	if (d)
		return ;
}

void	print_time_ls(t_ls *ls, t_flags_ls *d)
{
	time_t	file;
	time_t	today;

	today = time(NULL);
	if (d->u == 1)
		file = ls->filestat.st_atime;
	else
		file = ls->filestat.st_mtime;
	if (today - file < 15552000)
		ft_printf("%s %2d %02d:%02d ", ls->month, ls->day, ls->hour, ls->min);
	else
		ft_printf("%s %2d %5d ", ls->month, ls->day, ls->year);
}

void	print_error(t_ls *ls, t_flags_ls *d)
{
	t_ls	*tmp;

	tmp = ls;
	if (d->r == 1)
	{
		while (tmp->before != NULL)
			tmp = tmp->before;
	}
	while (tmp)
	{
		if (tmp->type == -1)
			error(0, tmp->path, 2);
		tmp = tmp->next;
	}
}

void	print_element_ls(t_ls *ls, t_flags_ls *d, t_ls *tmp, int *loop)
{
	ls = d->start_ls;
	if (*loop == 0)
		print_error(ls, d);
	while (ls)
	{
		if (ls->type == 0 && *loop == 1)
			display_ls(ls, d, 0);
		if (ls->type == 1 && *loop == 2)
			build_t_ls(check_slash_repo(ls->path), d, 1);
		tmp = ls;
		if (d->r == 0)
			ls = ls->next;
		else
			ls = ls->before;
		if (*loop == 2)
		{
			if (tmp->type == 0)
				free(tmp->path);
			ft_free_void(tmp->permission, tmp->uid, tmp->gid, tmp->month);
			free(tmp);
		}
	}
	++*loop;
}

void	print_args_ls(t_ls *ls, t_flags_ls *d, int i, char **argv)
{
	int		loop;
	t_ls	*tmp;

	loop = 0;
	while (argv[i])
	{
		ls = add_list_back_ls_next(ls, d);
		get_stat_ls(argv[i++], NULL, d, ls);
	}
	rebuild_before_list_ls(d->start_ls, &ls);
	d->start_ls = sort_list_ls(d->start_ls, d, &ls);
	if (d->r == 1)
	{
		while (ls->next)
			ls = ls->next;
		d->start_ls = ls;
	}
	tmp = ls;
	while (loop != 3)
		print_element_ls(ls, d, tmp, &loop);
}
