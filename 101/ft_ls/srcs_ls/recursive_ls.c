/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   recursive_ls.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rlegendr <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/02/22 10:25:19 by rlegendr     #+#   ##    ##    #+#       */
/*   Updated: 2019/02/25 15:22:59 by rlegendr    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

int		g_loop = 0;

int		get_name_index(char *path)
{
	int		i;

	i = ft_strlen(path) - 1;
	while (i >= 0 && path[i] != '/')
		i--;
	if (i == 0)
	{
		if (path[0] == '/')
			return (1);
		return (0);
	}
	return (i + 1);
}

char	*build_path(char *path, char *namefile)
{
	int		i;
	char	*ret;
	char	*tmp;

	ret = NULL;
	tmp = NULL;
	i = 0;
	if (ft_strcmp(namefile, ".") == 0 || ft_strcmp(namefile, "..") == 0)
		return (namefile);
	while (path[i])
		i++;
	if (path[--i] == '/')
	{
		path = ft_strjoin(namefile, "/");
		tmp = ft_strdup(path);
		free(path);
		return (tmp);
	}
	return (ft_strjoin(namefile, "/"));
}

void	upper_r_ls(t_ls *ls, t_flags_ls *d, char *arg)
{
	char	*newpath;

	newpath = NULL;
	while (ls != NULL)
	{
		if (ls->type == 1 && verif_ls_path(ls->path))
		{
			newpath = build_path(arg, ls->path);
			build_t_ls(newpath, d, 0);
		}
		ls = ls->next;
	}
}

void	build_t_ls(char *arg, t_flags_ls *d, int usage)
{
	t_ls	*ls;
	t_ls	*head;
	t_ls	*tmp;

	ls = NULL;
	tmp = NULL;
	if (g_loop++ != 0 || usage == 1)
		ft_printf("\n{U.}%s:\n", arg);
	head = read_dir_ls(arg, ls, d);
	if (head == NULL)
		return (free(arg));
	rebuild_before_list_ls(head, &ls);
	head = sort_list_ls(head, d, &ls);
	if (d->l == 1 && ls && (ls->next->next != NULL || d->a == 1))
		ft_printf("total {B.}%lu{eoc}\n", d->nb_block_tot);
	print_ls(ls, d, 1);
	reset_max_values_ls(d, head, &ls);
	if (d->upper_r == 1)
		upper_r_ls(ls, d, arg);
	free(arg);
	ls = head;
	free_t_ls(ls);
}
