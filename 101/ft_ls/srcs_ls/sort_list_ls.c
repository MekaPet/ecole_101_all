/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   sort_list_ls.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: bjuarez <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/02/18 13:39:42 by bjuarez      #+#   ##    ##    #+#       */
/*   Updated: 2019/02/22 10:24:51 by bjuarez     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

t_ls		*add_list_back_sort_ls(t_ls *head, t_ls *ls, int loop)
{
	t_ls	*tmp;

	tmp = NULL;
	if (loop == 0)
	{
		ls->next = NULL;
		return (ls);
	}
	tmp = head;
	while (head->next != NULL)
		head = head->next;
	head->next = ls;
	ls->next = NULL;
	ls->before = head;
	return (tmp);
}

void		generic_sort_list_3(t_ls **ls, t_ls **tmp)
{
	if (*tmp && *tmp == *ls)
		*ls = (*ls)->next;
	else
		(*tmp)->before->next = (*tmp)->next;
	if ((*tmp)->next == *ls)
		(*tmp)->before = NULL;
	else if ((*tmp)->next != NULL)
		(*tmp)->next->before = (*tmp)->before;
}

void		generic_sort_list_2(t_ls **tmp, t_ls **tmp2, t_flags_ls *d)
{
	while (*tmp2)
	{
		if (sort_by_type(*tmp, *tmp2, d) == 0)
			*tmp = *tmp2;
		*tmp2 = (*tmp2)->next;
	}
}

t_ls		*generic_sort_list(t_ls *head, t_ls *ls, t_flags_ls *d, int loop)
{
	t_ls	*tmp;
	t_ls	*tmp2;

	ls = head;
	tmp = ls;
	tmp2 = tmp;
	while (ls)
	{
		generic_sort_list_2(&tmp, &tmp2, d);
		if (ls->next == NULL)
		{
			head = add_list_back_sort_ls(head, ls, loop);
			ls = NULL;
		}
		else
		{
			generic_sort_list_3(&ls, &tmp);
			head = add_list_back_sort_ls(head, tmp, loop++);
			tmp = ls;
			tmp2 = tmp->next;
		}
	}
	return (head);
}

t_ls		*sort_list_ls(t_ls *head, t_flags_ls *d, t_ls **ls)
{
	t_ls	*ls_sort;

	ls_sort = NULL;
	if (head)
		head = generic_sort_list(head, ls_sort, d, 0);
	head->before = NULL;
	*ls = head;
	return (head);
}
