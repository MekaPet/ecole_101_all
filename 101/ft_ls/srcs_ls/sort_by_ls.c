/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   sort_by_ls.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: bjuarez <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/02/22 10:23:46 by bjuarez      #+#   ##    ##    #+#       */
/*   Updated: 2019/02/25 11:09:40 by rlegendr    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

int			ascii(t_ls *tmp, t_ls *tmp2)
{
	if (ft_strcmp(tmp->path, tmp2->path) <= 0)
		return (1);
	return (0);
}

int			sort_by_type_2(t_ls **tmp, t_ls **tmp2, t_flags_ls *d)
{
	if (d->upper_s == 1)
	{
		if ((*tmp)->size >= (*tmp2)->size)
			return (1);
		return (0);
	}
	if (d->f == 0 && d->t == 0 && d->upper_s == 0)
	{
		if (ft_strcmp((*tmp)->path, (*tmp2)->path) <= 0)
			return (1);
		return (0);
	}
	return (1);
}

int			sort_by_time_ls(t_ls *tmp, t_ls *tmp2, t_flags_ls *d)
{
	time_t	time1;
	time_t	time2;

	if (d->u)
	{
		time1 = tmp->filestat.st_atime;
		time2 = tmp2->filestat.st_atime;
	}
	else
	{
		time1 = tmp->filestat.st_mtime;
		time2 = tmp2->filestat.st_mtime;
	}
	if (time1 >= time2)
	{
		if (time1 == time2)
			return (ascii(tmp, tmp2));
		return (1);
	}
	return (0);
}

int			sort_by_type(t_ls *tmp, t_ls *tmp2, t_flags_ls *d)
{
	if (tmp->type == -1 || tmp2->type == -1)
	{
		if (tmp->type <= tmp2->type)
		{
			if (tmp->type == tmp2->type)
				return (ascii(tmp, tmp2));
			return (1);
		}
		return (0);
	}
	if (sort_by_type_2(&tmp, &tmp2, d) == 0)
		return (0);
	if (d->t == 1)
		return (sort_by_time_ls(tmp, tmp2, d));
	return (1);
}
