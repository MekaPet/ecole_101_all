/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   parsing_ls.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rlegendr <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/02/22 10:34:03 by rlegendr     #+#   ##    ##    #+#       */
/*   Updated: 2019/02/25 15:24:23 by rlegendr    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

void	aie(char *str)
{
	ft_printf("\nerror: %s\n", str);
	exit(0);
}

void	parsing_flags(t_flags_ls *d, char c)
{
	if (c == 'G')
		d->upper_g = 1;
	if (c == 'S')
		d->upper_s = 1;
	if (c == 'R')
		d->upper_r = 1;
	if (c == 'a')
		d->a = 1;
	if (c == 'f')
		d->f = 1;
	if (c == 'g')
		d->g = 1;
	if (c == 'l')
		d->l = 1;
	if (c == 'r')
		d->r = 1;
	if (c == 't')
		d->t = 1;
	if (c == 'u')
		d->u = 1;
	if (c == 's')
		d->s = 1;
	if (c == 'o')
		d->o = 1;
	check_rules_ls(d);
}

void	error(char c, char *str, int usage)
{
	if (usage == 1)
	{
		ft_printf("ls: illegal option -- %c\nusage: ls [-GSRafglorstu]", c);
		ft_printf(" [file ...]\n");
		exit(0);
	}
	if (usage == 2)
	{
		ft_printf("ls: %s: No such file or directory\n", str);
		free(str);
	}
	if (usage == 3)
		ft_printf("ls: %s: Permission denied\n", str);
}

void	get_flags_ls(t_flags_ls *d, char *str)
{
	char	*flags;
	int		i;

	i = 0;
	flags = "tarlufgsoGSR";
	while (str[++i])
	{
		if (ft_strchr(flags, str[i]) == NULL)
			error(str[i], NULL, 1);
		parsing_flags(d, str[i]);
	}
}

t_ls	*read_dir_ls(char *arg, t_ls *ls, t_flags_ls *d)
{
	DIR				*dirp;
	struct dirent	*read;
	int				loop;
	t_ls			*tmp;

	loop = 0;
	dirp = opendir(arg);
	if (dirp == NULL)
	{
		perror("ft_ls");
		return (NULL);
	}
	while ((read = readdir(dirp)) != NULL)
	{
		ls = add_list_back_ls_next(ls, d);
		get_stat_ls(arg, read->d_name, d, ls);
		d->nb_block_tot += ls->nb_block;
		if (loop++ == 0)
			tmp = ls;
	}
	closedir(dirp);
	return (tmp);
}
