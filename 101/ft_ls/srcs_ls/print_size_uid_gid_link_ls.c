/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   print_size_uid_gid_link_ls.c                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: bjuarez <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/02/22 10:25:40 by bjuarez      #+#   ##    ##    #+#       */
/*   Updated: 2019/02/25 09:57:01 by rlegendr    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

void	print_size_ls(t_ls *ls, t_flags_ls *d)
{
	int		nb_size;
	int		i;
	int		size2;
	int		res;

	i = 0;
	res = 0;
	nb_size = check_num_length(ls->size);
	if (d->nb_char_maj != -1 && d->nb_char_min != -1)
	{
		size2 = d->nb_char_maj + d->nb_char_min + 2 - d->nb_char_size;
		if (size2 < 0)
			size2 = 0;
		res = d->nb_char_size + size2;
	}
	else
		res = d->nb_char_size;
	while (nb_size++ < res)
		ft_putchar(' ');
	ft_printf("{B.}%lld{eoc} ", ls->size);
}

void	print_uid_ls(t_ls *ls, t_flags_ls *d)
{
	int		nb_uid;

	nb_uid = ft_strlen(ls->uid);
	ft_printf("{B.T.blue.}%s{eoc}  ", ls->uid);
	while (nb_uid++ < d->nb_char_uid)
		ft_putchar(' ');
}

void	print_gid_ls(t_ls *ls, t_flags_ls *d)
{
	int		nb_gid;

	nb_gid = ft_strlen(ls->gid);
	ft_printf("{B.T.grey.}%s{eoc}  ", ls->gid);
	while (nb_gid++ < d->nb_char_gid)
		ft_putchar(' ');
}

void	print_nb_link_ls(t_ls *ls, t_flags_ls *d)
{
	int		nb_link;

	nb_link = check_num_length(ls->nb_link);
	while (nb_link++ < d->nb_char_nb_link)
		ft_putchar(' ');
	ft_printf("{B.}%d{eoc} ", ls->nb_link);
}

void	print_link_ls(t_ls *ls)
{
	char	*buf;
	int		nb_link_printable;
	int		i;

	i = 0;
	if (!(buf = (char*)malloc(4096)))
		aie("buf link maloc failed");
	nb_link_printable = readlink(ls->path, buf, 4096);
	ft_putstr(" -> ");
	if (nb_link_printable == -1)
		perror("nb_link");
	else
	{
		while (i <= nb_link_printable)
			ft_putchar(buf[i++]);
	}
	free(buf);
}
