/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   op_struct_ls.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rlegendr <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/02/22 10:33:01 by rlegendr     #+#   ##    ##    #+#       */
/*   Updated: 2019/02/22 10:36:11 by rlegendr    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

t_ls	*add_list_back_ls_next(t_ls *ls, t_flags_ls *d)
{
	t_ls	*new;

	new = NULL;
	if (!(new = (t_ls*)malloc(sizeof(t_ls))))
		aie("rate -1-\n");
	if (ls == NULL)
	{
		init_t_ls(new);
		d->start_ls = new;
		new->first_chain = 1;
		return (new);
	}
	else
	{
		while (ls->next != NULL)
			ls = ls->next;
		init_t_ls(new);
		ls->next = new;
		return (new);
	}
	free(new);
	return (NULL);
}

void	rebuild_before_list_ls(t_ls *start_ls, t_ls **ls)
{
	t_ls	*tmp;

	*ls = start_ls;
	tmp = NULL;
	if (start_ls)
		tmp = start_ls->next;
	if (tmp == NULL)
		return ;
	while (tmp)
	{
		tmp->before = start_ls;
		tmp = tmp->next;
		start_ls = start_ls->next;
	}
}
