/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   tools_ls.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rlegendr <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/02/22 10:21:43 by rlegendr     #+#   ##    ##    #+#       */
/*   Updated: 2019/02/26 08:14:48 by rlegendr    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

char	*remove_dot_slash(char *str)
{
	int		i;
	int		j;
	char	*ans;

	j = 0;
	ans = NULL;
	i = ft_strlen(str);
	if (i < 1 || str[0] != '.' || str[1] != '/')
		return (ft_strdup(str));
	ans = (char*)malloc(sizeof(char) * i + 1 - 2);
	ans = ft_memmove(ans, str + 2, i);
	return (ans);
}

int		finish_by_slash(char *str)
{
	int		i;

	i = ft_strlen(str);
	if (str[i - 1] == '/')
		return (1);
	return (0);
}

char	*check_slash_repo(char *path)
{
	int		i;
	char	*tmp;

	tmp = path;
	i = ft_strlen(path) - 1;
	if (i >= 0 && path[i] == '/')
		return (path);
	else if (i >= 0)
	{
		path = ft_strjoin(path, "/");
		free(tmp);
		return (path);
	}
	aie("check slash repo failed");
	exit(0);
	return (NULL);
}

int		verif_ls_path(char *ls_path)
{
	int		i;

	i = ft_strlen(ls_path);
	i--;
	if (i >= 1 && ls_path[i] == '.' && ls_path[i - 1] == '/')
		return (0);
	if (i >= 2 && ls_path[i] == '.' &&
			ls_path[i - 1] == '.' && ls_path[i - 2] == '/')
		return (0);
	while (i >= 0 && ls_path[i] != '/')
		i--;
	if (ls_path[i + 1] == '.')
		return (0);
	return (1);
}

void	free_t_ls(t_ls *ls)
{
	t_ls	*tmp;

	tmp = ls;
	if (ls == NULL)
		return ;
	while (ls != NULL)
	{
		tmp = ls;
		ls = ls->next;
		ft_free_void(tmp->path, tmp->uid, tmp->gid, tmp->permission);
		ft_free_void(tmp->month, tmp, NULL, NULL);
	}
}
