/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   initialisation_rules_ls.c                        .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rlegendr <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/02/22 10:29:30 by rlegendr     #+#   ##    ##    #+#       */
/*   Updated: 2019/02/25 12:49:52 by rlegendr    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

void	check_rules_ls(t_flags_ls *d)
{
	if (d->g == 1)
		d->l = 1;
	if (d->f == 1)
	{
		d->a = 1;
		d->upper_s = 0;
		d->t = 0;
	}
	if (d->upper_s == 1)
		d->t = 0;
}

void	init_flags_ls(t_flags_ls *d)
{
	d->t = 0;
	d->a = 0;
	d->r = 0;
	d->l = 0;
	d->u = 0;
	d->f = 0;
	d->g = 0;
	d->s = 0;
	d->o = 0;
	d->upper_g = 1;
	d->upper_s = 0;
	d->upper_r = 0;
	d->total = 0;
	d->nb_block_tot = 0;
	d->nb_char_nb_link = 0;
	d->nb_char_size = 0;
	d->nb_char_uid = 0;
	d->nb_char_gid = 0;
	d->nb_char_maj = -1;
	d->nb_char_min = -1;
	d->nb_char_block = 0;
}

void	init_t_ls(t_ls *ls)
{
	ls->first_chain = 0;
	ls->stat = 0;
	ls->type = 404;
	ls->path = NULL;
	ls->permission = NULL;
	ls->nb_link = 0;
	ls->inode = 0;
	ls->pwuid = NULL;
	ls->uid = NULL;
	ls->grgid = NULL;
	ls->gid = NULL;
	ls->size = 0;
	ls->nb_block = 0;
	ls->year = 0;
	ls->month = NULL;
	ls->day = 0;
	ls->hour = 0;
	ls->min = 0;
	ls->sec = 0;
	ls->dev_maj = -1;
	ls->dev_min = -1;
	ls->total_sec = 0;
	ls->next = NULL;
	ls->before = NULL;
}

void	reset_max_values_ls(t_flags_ls *d, t_ls *head, t_ls **ls)
{
	d->nb_block_tot = 0;
	d->nb_char_size = 0;
	d->nb_char_nb_link = 0;
	d->nb_char_uid = 0;
	d->nb_char_gid = 0;
	*ls = head;
}
