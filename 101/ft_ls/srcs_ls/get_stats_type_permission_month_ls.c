/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   get_stats_type_permission_month_ls.c             .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: bjuarez <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/02/22 10:29:44 by bjuarez      #+#   ##    ##    #+#       */
/*   Updated: 2019/02/26 08:17:23 by rlegendr    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

void	get_nb_block(t_ls *ls, t_flags_ls *d)
{
	int		nb_char_block;

	ls->nb_block = ls->filestat.st_blocks;
	nb_char_block = check_num_length(ls->nb_block);
	if (d->nb_char_block < nb_char_block)
		d->nb_char_block = nb_char_block;
}

int		get_type_ls(t_ls *ls)
{
	if (ls->filestat.st_mode & S_IFDIR)
		return (1);
	return (0);
}

char	*get_type_permission_ls(char *permission, t_ls *ls)
{
	if (ls->filestat.st_mode & S_IFIFO)
		permission[0] = 'p';
	if (ls->filestat.st_mode & S_IFCHR)
		permission[0] = 'c';
	if (ls->filestat.st_mode & S_IFDIR)
		permission[0] = 'd';
	if (ls->filestat.st_mode & S_IFREG)
		permission[0] = '-';
	if (ls->filestat.st_mode & S_IFCHR && ls->filestat.st_mode & S_IFDIR)
		permission[0] = 'b';
	if (ls->filestat.st_mode & S_IFREG && ls->filestat.st_mode & S_IFCHR)
		permission[0] = 'l';
	if (ls->filestat.st_mode & S_IFREG && ls->filestat.st_mode & S_IFDIR)
		permission[0] = 's';
	permission[10] = ' ';
	permission[11] = '\0';
	return (permission);
}

char	*get_permission_ls(t_ls *ls, char *permission)
{
	if (!(permission = (char*)malloc(sizeof(char) * 12)))
		aie("permission failed");
	get_type_permission_ls(permission, ls);
	permission[1] = (ls->filestat.st_mode & S_IRUSR) ? 'r' : '-';
	permission[2] = (ls->filestat.st_mode & S_IWUSR) ? 'w' : '-';
	permission[3] = (ls->filestat.st_mode & S_IXUSR) ? 'x' : '-';
	if (permission[3] != 'x')
		permission[3] = (ls->filestat.st_mode & 04000) ? 'S' : permission[3];
	else
		permission[3] = (ls->filestat.st_mode & 04000) ? 's' : permission[3];
	permission[4] = (ls->filestat.st_mode & S_IRGRP) ? 'r' : '-';
	permission[5] = (ls->filestat.st_mode & S_IWGRP) ? 'w' : '-';
	permission[6] = (ls->filestat.st_mode & S_IXGRP) ? 'x' : '-';
	if (permission[6] != 'x')
		permission[6] = (ls->filestat.st_mode & 02000) ? 'S' : permission[6];
	else
		permission[6] = (ls->filestat.st_mode & 02000) ? 's' : permission[6];
	permission[7] = (ls->filestat.st_mode & S_IROTH) ? 'r' : '-';
	permission[8] = (ls->filestat.st_mode & S_IWOTH) ? 'w' : '-';
	permission[9] = (ls->filestat.st_mode & S_IXOTH) ? 'x' : '-';
	if (permission[9] != 'x')
		permission[9] = (ls->filestat.st_mode & 01000) ? 'T' : permission[9];
	else
		permission[9] = (ls->filestat.st_mode & 01000) ? 't' : permission[9];
	return (permission);
}
