/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   main.c                                           .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rlegendr <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/02/22 10:20:59 by rlegendr     #+#   ##    ##    #+#       */
/*   Updated: 2019/02/25 12:47:44 by rlegendr    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

int		main(int argc, char **argv)
{
	t_flags_ls	d;
	int			i;
	char		*current;
	t_ls		*ls;

	ls = NULL;
	current = ft_strdup("./");
	i = 0;
	argc = 0;
	init_flags_ls(&d);
	while (argv[++i] && argv[i][0] == '-' && argv[i][1])
		get_flags_ls(&d, argv[i]);
	if (argv[i] == NULL)
		build_t_ls(current, &d, 0);
	else
	{
		print_args_ls(ls, &d, i, argv);
		free(current);
	}
	return (0);
}
