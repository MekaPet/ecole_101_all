/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   get_stats_time_dev_stat_ls.c                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: bjuarez <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/02/22 10:31:53 by bjuarez      #+#   ##    ##    #+#       */
/*   Updated: 2019/02/26 08:17:42 by rlegendr    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

void	get_time_ls(t_ls *ls, t_flags_ls *d)
{
	time_t		time;
	char		*time_str;

	if (d->u == 1)
		time = ls->filestat.st_atime;
	else
		time = ls->filestat.st_mtime;
	time_str = ctime(&time);
	ls->year = ft_atoi(time_str + 20);
	ls->month = ft_strndup(time_str + 4, 3);
	ls->day = ft_atoi(time_str + 8);
	ls->hour = ft_atoi(time_str + 11);
	ls->min = ft_atoi(time_str + 14);
	ls->sec = ft_atoi(time_str + 17);
}

void	get_dev_values_ls(t_ls *ls, t_flags_ls *d)
{
	int		nb_dev_min;
	int		nb_dev_maj;

	nb_dev_min = 0;
	nb_dev_maj = 0;
	if (ls->permission[0] != 'c' && ls->permission[0] != 'b')
		return ;
	ls->dev_maj = major(ls->filestat.st_rdev);
	ls->dev_min = minor(ls->filestat.st_rdev);
	nb_dev_min = check_num_length(ls->dev_min);
	nb_dev_maj = check_num_length(ls->dev_maj);
	if (d->nb_char_maj < nb_dev_maj)
		d->nb_char_maj = nb_dev_maj;
	if (d->nb_char_min < nb_dev_min)
		d->nb_char_min = nb_dev_min;
}

void	get_nb_char(t_flags_ls *d, t_ls *ls, int nb_size)
{
	int		nb_link;
	int		nb_uid;
	int		nb_gid;

	nb_link = 0;
	nb_uid = 0;
	nb_gid = 0;
	if (verif_ls_path(ls->path) == 0 && d->a == 0)
		return ;
	nb_size = check_num_length(ls->size);
	nb_link = check_num_length(ls->nb_link);
	if (ls->uid)
		nb_uid = ft_strlen(ls->uid);
	if (ls->gid)
		nb_gid = ft_strlen(ls->gid);
	if (d->nb_char_size < nb_size)
		d->nb_char_size = nb_size;
	if (d->nb_char_nb_link < nb_link)
		d->nb_char_nb_link = nb_link;
	if (d->nb_char_uid < nb_uid)
		d->nb_char_uid = nb_uid;
	if (d->nb_char_gid < nb_gid)
		d->nb_char_gid = nb_gid;
	get_dev_values_ls(ls, d);
}

void	get_valid_stat(t_ls *ls, t_flags_ls *d)
{
	ls->permission = get_permission_ls(ls, NULL);
	ls->nb_link = ls->filestat.st_nlink;
	ls->pwuid = getpwuid(ls->filestat.st_uid);
	if (ls->pwuid)
		ls->uid = ft_strdup(ls->pwuid->pw_name);
	else
		ls->uid = ft_itoa((long)ls->filestat.st_uid);
	ls->grgid = getgrgid(ls->filestat.st_gid);
	if (ls->grgid)
		ls->gid = ft_strdup(ls->grgid->gr_name);
	else
		ls->gid = ft_itoa((long)ls->filestat.st_gid);
	get_nb_block(ls, d);
	get_time_ls(ls, d);
	ls->size = ls->filestat.st_size;
	get_nb_char(d, ls, 0);
}

void	get_stat_ls(char *arg, char *read, t_flags_ls *d, t_ls *ls)
{
	char	*tmp;

	tmp = NULL;
	if (read)
		tmp = ft_strjoin(arg, read);
	else
		tmp = ft_strdup(arg);
	ls->stat = lstat(tmp, &ls->filestat);
	ls->path = tmp;
	ls->type = get_type_ls(ls);
	if (ls->stat != -1)
		get_valid_stat(ls, d);
	ls->type = ls->stat == -1 ? -1 : ls->type;
}
