/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_ls.h                                          .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rlegendr <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/02/08 10:05:14 by rlegendr     #+#   ##    ##    #+#       */
/*   Updated: 2019/02/26 08:24:56 by rlegendr    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef FT_LS_H
# define FT_LS_H

# include "../libft/includes/libft.h"
# include <dirent.h>
# include <sys/stat.h>
# include <sys/types.h>
# include <pwd.h>
# include <uuid/uuid.h>
# include <grp.h>
# include <time.h>
# include <stdio.h>

typedef struct		s_ls
{
	int				first_chain;
	struct stat		filestat;
	int				stat;
	int				type;
	char			*path;
	char			*permission;
	int				nb_link;
	int				inode;
	struct passwd	*pwuid;
	char			*uid;
	struct group	*grgid;
	char			*gid;
	int				size;
	unsigned long	nb_block;
	int				year;
	char			*month;
	int				day;
	int				hour;
	int				min;
	int				sec;
	int				dev_maj;
	int				dev_min;
	long			total_sec;
	struct s_ls		*next;
	struct s_ls		*before;
}					t_ls;

typedef struct		s_flags_ls
{
	t_ls			*start_ls;
	unsigned long	nb_block_tot;
	int				nb_char_size;
	int				nb_char_nb_link;
	int				nb_char_uid;
	int				nb_char_gid;
	int				nb_char_maj;
	int				nb_char_min;
	int				nb_char_block;
	int				t;
	int				a;
	int				r;
	int				l;
	int				u;
	int				f;
	int				g;
	int				s;
	int				o;
	int				upper_g;
	int				upper_s;
	int				upper_r;
	int				total;
}					t_flags_ls;

/*
** sort_by_ls.c
*/

int					ascii(t_ls *tmp, t_ls *tmp2);
int					sort_by_type_2(t_ls **tmp, t_ls **tmp2, t_flags_ls *d);
int					sort_by_time_ls(t_ls *tmp, t_ls *tmp2, t_flags_ls *d);
int					sort_by_type(t_ls *tmp, t_ls *tmp2, t_flags_ls *d);

/*
** sort_list_ls.c
*/

t_ls				*add_list_back_sort_ls(t_ls *head, t_ls *ls, int loop);
void				generic_sort_list_3(t_ls **ls, t_ls **tmp);
void				generic_sort_list_2(t_ls **tmp, t_ls **tmp2, t_flags_ls *d);
t_ls				*generic_sort_list(t_ls *head, t_ls *ls,
						t_flags_ls *d, int loop);
t_ls				*sort_list_ls(t_ls *head, t_flags_ls *d, t_ls **ls);

/*
** print_size_uid_gid_link_ls.c
*/

void				print_size_ls(t_ls *ls, t_flags_ls *d);
void				print_uid_ls(t_ls *ls, t_flags_ls *d);
void				print_gid_ls(t_ls *ls, t_flags_ls *d);
void				print_nb_link_ls(t_ls *ls, t_flags_ls *d);
void				print_link_ls(t_ls *ls);

/*
** print_args_time_ls.c
*/

void				print_dev_ls(t_ls *ls, t_flags_ls *d);
void				print_time_ls(t_ls *ls, t_flags_ls *d);
void				print_error(t_ls *ls, t_flags_ls *d);
void				print_element_ls(t_ls *ls, t_flags_ls *d,
						t_ls *tmp, int *loop);
void				print_args_ls(t_ls *ls, t_flags_ls *d, int i, char **argv);

/*
** get_stats_type_permission_month_ls.c
*/

void				get_nb_block(t_ls *ls, t_flags_ls *d);
int					get_type_ls(t_ls *ls);
char				*get_type_permission_ls(char *permission, t_ls *ls);
char				*get_permission_ls(t_ls *ls, char *permission);

/*
** get_stats_time_dev_stat_ls.c
*/

void				get_time_ls(t_ls *ls, t_flags_ls *d);
void				get_dev_values_ls(t_ls *ls, t_flags_ls *d);
void				get_nb_char(t_flags_ls *d, t_ls *ls, int nb_size);
void				get_valid_start(t_ls *ls, t_flags_ls *d);
void				get_stat_ls(char *arg, char *read, t_flags_ls *d, t_ls *ls);

/*
** display_ls.c
*/

void				color_executable_path(t_ls *ls, int name_index);
void				color_path_ls(t_ls *ls, t_flags_ls *d, int usage);
void				print_nb_block(t_ls *ls, t_flags_ls *d);
void				display_ls(t_ls *ls, t_flags_ls *d, int usage);
void				print_ls(t_ls *ls, t_flags_ls *d, int usage);

/*
** parsing_ls.c
*/

void				aie(char *str);
void				parsing_flags(t_flags_ls *d, char c);
void				error(char c, char *str, int usage);
void				get_flags_ls(t_flags_ls *d, char *str);
t_ls				*read_dir_ls(char *arg, t_ls *ls, t_flags_ls *d);

/*
** op_struct_ls.c
*/

t_ls				*add_list_back_ls_next(t_ls *ls, t_flags_ls *d);
void				rebuild_before_list_ls(t_ls *start_ls, t_ls **ls);

/*
** initialisation_rules_ls.c
*/

void				check_rules_ls(t_flags_ls *d);
void				init_flags_ls(t_flags_ls *d);
void				init_t_ls(t_ls *ls);
void				reset_max_values_ls(t_flags_ls *d, t_ls *head, t_ls **ls);

/*
** recursive_ls.c
*/

int					get_name_index(char *path);
char				*build_path(char *path, char *namefile);
void				upper_r_ls(t_ls *ls, t_flags_ls *d, char *arg);
void				build_t_ls(char *arg, t_flags_ls *d, int usage);

/*
** tools_ls.c
*/

char				*remove_dot_slash(char *str);
int					finish_by_slash(char *str);
char				*check_slash_repo(char *path);
int					verif_ls_path(char *ls_path);
void				free_t_ls(t_ls *ls);

/*
** main_ls.c
*/

int					main(int argc, char **argv);

#endif
