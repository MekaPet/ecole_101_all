/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   main.c                                           .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: bjuarez <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/06 15:32:25 by bjuarez      #+#   ##    ##    #+#       */
/*   Updated: 2018/11/06 15:40:33 by bjuarez     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "get_next_line.h"
#include <fcntl.h>

int		main(int argc, char **argv)
{
	int		fd;
	char	*line;

	if (argc == 1)
		fd = 0;
	else if (argc == 2)
		fd = open(argv[1], O_RDONLY);
	else
		return (2);
	while (get_next_line(fd, &line) == 1)
	{
		ft_putendl(line);
		free(line);
	}
	if (argc == 2)
		close(fd);
}

/*
int		main(int argc, char **argv)
{
	int		fd;
	int 	fd2;
	int		fd3;
	int		fd4;
	char	*line;

	ac = 0;
	fd = open(argv[1], O_RDONLY);
	fd2 = open(argv[2], O_RDONLY);
	fd3 = open(argv[3], O_RDONLY);
	fd4 = open(argv[4], O_RDONLY);

	printf("%d = fd -- line = %s\n", get_next_line(fd, &line), line);
	printf("%d = fd2 -- line = %s\n", get_next_line(fd2, &line), line);
	printf("%d = fd3 -- line = %s\n", get_next_line(fd3, &line), line);
	printf("%d = fd4 -- line = %s\n", get_next_line(fd4, &line), line);
	printf("%d = fd -- line = %s\n", get_next_line(fd, &line), line);
	printf("%d = fd2 -- line = %s\n", get_next_line(fd2, &line), line);
	printf("%d = fd3 -- line = %s\n", get_next_line(fd3, &line), line);
	printf("%d = fd4 -- line = %s\n", get_next_line(fd4, &line), line);
	printf("%d = fd -- line = %s\n", get_next_line(fd, &line), line);
	printf("%d = fd2 -- line = %s\n", get_next_line(fd2, &line), line);
	printf("%d = fd3 -- line = %s\n", get_next_line(fd3, &line), line);
	printf("%d = fd4 -- line = %s\n", get_next_line(fd4, &line), line);
	printf("%d = fd -- line = %s\n", get_next_line(fd, &line), line);
	printf("%d = fd2 -- line = %s\n", get_next_line(fd2, &line), line);
	printf("%d = fd3 -- line = %s\n", get_next_line(fd3, &line), line);
	printf("%d = fd4 -- line = %s\n", get_next_line(fd4, &line), line);
	close(fd);
	close(fd2);
	close(fd3);
	close(fd4);
}
*/
