/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   display_file.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: bjuarez <bjuarez@student.le-101.fr>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/02 14:07:50 by bjuarez      #+#   ##    ##    #+#       */
/*   Updated: 2018/10/02 16:57:38 by bjuarez     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "../includes/ft_inc.h"

void		ft_putstr(char *str)
{
	int		len;

	len = 0;
	while (str[len])
		len++;
	write(1, str, len);
}

void		ft_puterr(char *str)
{
	int		len;

	len = 0;
	while (str[len])
		len++;
	write(2, str, len);
}

void		ft_print_err(int ac)
{
	if (ac == 1)
		ft_puterr("File name missing.\n");
	else if (ac > 2)
		ft_puterr("Too many arguments.\n");
}

int			main(int argc, char **argv)
{
	int		i;
	int		fd;
	int		ret;
	char	buf[BUF_SIZE + 1];

	i = 1;
	if (argc == 1 || argc > 2)
	{
		ft_print_err(argc);
		return (0);
	}
	while (argv[i])
	{
		fd = open(argv[i], O_RDWR);
		if (fd >= 0)
		{
			while ((ret = read(fd, buf, BUF_SIZE)))
				ft_putstr(buf);
			buf[ret] = '\0';
			close(fd);
		}
		i++;
	}
	return (0);
}
