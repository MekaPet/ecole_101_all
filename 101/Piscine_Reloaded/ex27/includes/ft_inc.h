/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_inc.h                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: bjuarez <bjuarez@student.le-101.fr>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/02 13:58:34 by bjuarez      #+#   ##    ##    #+#       */
/*   Updated: 2018/10/02 13:58:58 by bjuarez     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef FT_INC_H
# define FT_INC_H
# include <sys/types.h>
# include <sys/stat.h>
# include <sys/uio.h>
# include <fcntl.h>
# include <unistd.h>
# define BUF_SIZE 1
#endif
