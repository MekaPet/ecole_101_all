/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_iterative_factorial.c                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: bjuarez <bjuarez@student.le-101.fr>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/01 15:57:56 by bjuarez      #+#   ##    ##    #+#       */
/*   Updated: 2018/10/02 12:26:25 by bjuarez     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

int	ft_iterative_factorial(int nb)
{
	int facto;

	facto = 0;
	if (nb >= 0 && nb <= 12)
	{
		if (nb == 0)
			return (1);
		facto = nb;
		while (nb > 1)
		{
			nb--;
			facto = facto * nb;
		}
	}
	return (facto);
}
