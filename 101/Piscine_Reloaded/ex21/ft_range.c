/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_range.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: bjuarez <bjuarez@student.le-101.fr>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/02 08:33:27 by bjuarez      #+#   ##    ##    #+#       */
/*   Updated: 2018/10/02 16:38:05 by bjuarez     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_absolute_diff(int min, int max)
{
	int	difference;

	difference = max - min;
	if (difference < 0)
		difference *= -1;
	return (difference);
}

int		*ft_range(int min, int max)
{
	int	i;
	int	diff;
	int	*table;

	i = 0;
	diff = ft_absolute_diff(min, max);
	table = (int*)malloc(sizeof(*table) * (diff));
	if (min >= max)
		return (NULL);
	while (i < diff)
	{
		table[i] = min;
		i++;
		min++;
	}
	table[i] = '\0';
	return (table);
}
