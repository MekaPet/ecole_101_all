/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_putnstrn.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: bjuarez <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/11 15:44:39 by bjuarez      #+#   ##    ##    #+#       */
/*   Updated: 2018/10/11 15:52:20 by bjuarez     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

void	ft_putnstrn(char *str, int b, int f)
{
	if (b >= 0)
	{
		while (str[b] && (b < f))
		{
			ft_putchar(str[b]);
			b++;
		}
	}
}
