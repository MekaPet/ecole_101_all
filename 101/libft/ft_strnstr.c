/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strnstr.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: bjuarez <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/10 09:41:32 by bjuarez      #+#   ##    ##    #+#       */
/*   Updated: 2018/10/10 09:41:33 by bjuarez     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strnstr(const char *haystack, const char *needle, size_t len)
{
	int		i;
	int		j;
	char	*str_haystack;

	i = 0;
	str_haystack = (char*)haystack;
	if ((ft_strlen(str_haystack) < ft_strlen(needle)))
		return (NULL);
	if (ft_strlen(needle) == 0 || (ft_strlen(needle) == 0 &&
				ft_strlen(str_haystack) == 0))
		return (str_haystack);
	while (str_haystack[i])
	{
		j = 0;
		while (str_haystack[i + j] == needle[j] && (i + j) < (int)len)
		{
			if (needle[j + 1] == '\0')
				return (str_haystack + i);
			j++;
		}
		i++;
	}
	return (NULL);
}
