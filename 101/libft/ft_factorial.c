/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_factorial.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: bjuarez <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/12 09:30:22 by bjuarez      #+#   ##    ##    #+#       */
/*   Updated: 2018/10/12 09:31:02 by bjuarez     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

int	ft_factorial(int nb)
{
	int	facto;

	facto = 0;
	if (nb >= 0 || nb > 12)
	{
		if (nb == 0)
			return (1);
		facto = nb;
		if (nb > 1)
		{
			nb--;
			facto = facto * ft_factorial(nb);
		}
	}
	else
		facto = 0;
	return (facto);
}
