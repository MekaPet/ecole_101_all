/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strrchr.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: bjuarez <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/10 09:41:46 by bjuarez      #+#   ##    ##    #+#       */
/*   Updated: 2018/10/10 16:38:18 by bjuarez     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strrchr(const char *s, int c)
{
	int		i;
	char	*str_s;

	str_s = (char*)s;
	i = ft_strlen(str_s);
	while (i >= 0)
	{
		if (str_s[i] == (char)c)
			return (&str_s[i]);
		i--;
	}
	if (c == 0 && s)
		return (&str_s[ft_strlen(s)]);
	return (NULL);
}
