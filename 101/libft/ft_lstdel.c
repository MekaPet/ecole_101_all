/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_lstdel.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: bjuarez <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/10 12:35:28 by bjuarez      #+#   ##    ##    #+#       */
/*   Updated: 2018/10/10 15:16:05 by bjuarez     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	t_list *temp;
	t_list *next;

	if (alst && del)
	{
		temp = *alst;
		while (temp)
		{
			next = temp->next;
			del((temp)->content, (temp)->content_size);
			free(temp);
			temp = next;
		}
		*alst = NULL;
	}
}
