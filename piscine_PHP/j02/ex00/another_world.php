#!/usr/bin/php
<?php
if ($argc == 1)
	exit;
$tab = preg_split('/[\ \t]+/', $argv[1], -1, PREG_SPLIT_NO_EMPTY);
$i = 0;
foreach ($tab as $string)
{
	if ($i == 0)
		echo "$string";
	else
		echo " $string";
	$i++;
}
echo "\n";
?>
