#!/usr/bin/php
<?php
if ($argc < 2 || !file_exists($argv[1]))
	exit;
$read = fopen($argv[1], 'r');
$line = "";
while ($read && !feof($read))
	$line .= fgets($read);
function replace($arg)
{
	return ($arg[1].strtoupper($arg[2]).$arg[3]);
}
$line = preg_replace_callback("/(<.*title=\")(.*)(\">)/i", "replace", $line);
$line = preg_replace_callback("/(<a.*>)(.*)(<\/a)/i", "replace", $line);
$line = preg_replace_callback("/(<a.*>)(.*)(<img)/i", "replace", $line);
$line = preg_replace_callback("/(<a.*><img.*>)(.*)(<\/a>)/i", "replace", $line);
echo $line;
?>
