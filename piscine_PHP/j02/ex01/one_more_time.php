#!/usr/bin/php
<?php
if ($argc < 2)
	exit;
date_default_timezone_set('Europe/Paris');
$tab = preg_match("#^[A-Z]?[a-z]{4,8}\ \d{1,2}\ [A-Z]?[a-z]{2,8}\ \d{4}\ \d{2}:\d{2}:\d{2}$#", $argv[1]);
if ($tab == 0)
{
	echo "Wrong Format\n";
	exit;
}
$tab = str_replace(":", " ", $argv[1]);
$tab = explode(" ", $tab);

$jour = $tab[1];
$annee = $tab[3];
$heure = $tab[4];
$min = $tab[5];
$s = $tab[6];
if ($tab[2] === "Janvier" || $tab[2] === "janvier")
	$mois = 1;
if ($tab[2] === "Fevrier" || $tab[2] === "fevrier")
	$mois = 2;
if ($tab[2] === "Mars" || $tab[2] === "mars")
	$mois = 3;
if ($tab[2] === "Avril" || $tab[2] === "avril")
	$mois = 4;
if ($tab[2] === "Mai" || $tab[2] === "mai")
	$mois = 5;
if ($tab[2] === "Juin" || $tab[2] === "juin")
	$mois = 6;
if ($tab[2] === "Juillet" || $tab[2] === "juillet")
	$mois = 7;
if ($tab[2] === "Aout" || $tab[2] === "aout")
	$mois = 8;
if ($tab[2] === "Septembre" || $tab[2] === "septembre")
	$mois = 9;
if ($tab[2] === "Octobre" || $tab[2] === "octobre")
	$mois = 10;
if ($tab[2] === "Novembre" || $tab[2] === "novembre")
	$mois = 11;
if ($tab[2] === "Decembre" || $tab[2] === "decembre")
	$mois = 12;
$time_res = mktime($heure, $min, $s, $mois, $jour, $annee);
echo $time_res."\n";
?>
