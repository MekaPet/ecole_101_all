<?php
class UnholyFactory {
	private $_templates;

	public function __construct() {
		$this->_templates = array();
	}

	public function absorb($instanceFighter) {
		if ($instanceFighter instanceof Fighter) {
			$nameFighter = $instanceFighter->__tostring();
			if (in_array($name, $this->_templates) == false) {
				print("(Factory absorbed a fighter of type ".$nameFighter.")\n");
				$this->_templates[get_class($instanceFighter)] = $nameFighter;
			}
			else
				print("(Factory already absorbed a fighter of type ".$nameFighter.")\n");
		}
		else
			print("(Factory can't absorb this, it's not a fighter)\n");
	}

	public function fabricate($nameRequestFighter) {
		if (in_array($nameRequestFighter, $this->_templates) == false) {
			print("(Factory hasn't absorbed any fighter of type $nameRequestFighter)\n");
			return NULL;
		}
		else
		{
			print("(Factory fabricates a fighter of type $nameRequestFighter)\n");
			$key = array_search($nameRequestFighter, $this->_templates);
			return(new $key());
		}
	}
}
?>
