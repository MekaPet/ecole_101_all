<?php
class Fighter {
	private $_name;

	public function __construct($arg) {
		if ($arg)
			$this->_name = $arg;
		else
			$this->_name = NULL;
	}
	public function __tostring() {
		if ($this->_name)
			return ($this->_name);
	}
}
?>
