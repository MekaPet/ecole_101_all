<?php
	require_once('Vertex.class.php');
class Vector {
	private $_x;
	private $_y;
	private $_z;
	private $_w = 0.0;
	static $verbose = false;

	public function __construct ( array $kwargs ) {
		if ( isset( $kwargs['orig'] ) )
		{
			$orig = new Vertex( array(
				'x' => $kwargs['orig']->_x,
				'y' => $kwargs['orig']->_y,
				'z' => $kwargs['orig']->_z ) );
		}
		else if ( !isset( $orig ) ) {
			$orig = new Vertex( array(
				'x' => 0.0,
				'y' => 0.0,
				'z' => 0.0,
				'w' => 1.0 ) );
		}
		if ( isset( $kwargs['dest'] ) ) {
			$this->_x = $kwargs['dest']->_x - $kwargs['orig']->_x;
			$this->_y = $kwargs['dest']->_y - $kwargs['orig']->_y;
			$this->_z = $kwargs['dest']->_z - $kwargs['orig']->_z;
		}
		if ( self::$verbose )
			printf( $this . " contructed\n" );
	}

	public function __destruct () {
		if ( self::$verbose )
			printf( $this . " destructed\n" );
	}

	public function __tostring() {
		$ret = sprintf( "Vector( x: %.2f, y: %.2f, z:%.2f, w:%.2f )", $this->_x, $this->_y, $this->_z, $this->_w );
		return $ret;
	}

	public function doc() {
		if ( $str = file_get_contents( 'Vector.doc.txt' ) ) {
			echo $str;
		}
		else {
			echo "Error: .doc file doesn't exist.\n";
		}
	}

	public function magnitude() {
		$magn = (float)sqrt(
			( $this->_x - $orig->x ) ** 2 +
			( $this->_y - $orig->y ) ** 2 +
			( $this->_z - $orig->z ) ** 2 );
		if ( $magn == 1 )
			return ( "norme" );
		else
			return ( $magn );
	}

	public function normalize() {
		$l = $this->magnitude();
		if ( $l == 1 )
			return clone $this;
		$norme = new Vector( array( 'dest' => new Vertex( array (
			'x' => $this->_x / $l,
			'y' => $this->_y / $l,
			'z' => $this->_z / $l ) ) ) );
		return ( $norme );
	}

	public function add( Vector $rhs ) {
		$add = new Vector( array( 'dest' => new Vertex( array (
			'x' => $this->_x + $rhs->_x,
			'y' => $this->_y + $rhs->_y,
			'z' => $this->_z + $rhs->_z ) ) ) );
		return ( $add );
	}

	public function sub( Vector $rhs ) {
		$sub = new Vector( array( 'dest' => new Vertex( array (
			'x' => $this->_x - $rhs->_x,
			'y' => $this->_y - $rhs->_y,
			'z' => $this->_z - $rhs->_z ) ) ) );
		return ( $sub );
	}

	public function opposite() {
		$opp = new Vector( array( 'dest' => new Vertex( array (
			'x' => $this->_x * (-1),
			'y' => $this->_y * (-1),
			'z' => $this->_z * (-1) ) ) ) );
		return ( $opp );
	}

	public function scalarProduct( $k ) {
		$scalProd = new Vector( array( 'dest' => new Vertex( array (
			'x' => $this->_x * $k,
			'y' => $this->_y * $k,
			'z' => $this->_z * $k ) ) ) );
		return ( $scalProd );
	}

	public function dotProduct( Vector $rhs ) {
		$dotProd = (float)
			$this->_x * $rhs->_x +
			$this->_y * $rhs->_y +
			$this->_z * $rhs->_z;
		return ( $dotProd );
	}

	public function cos( Vector $rhs ) {
		if ( $this->magnitude() == "norme" || $rhs->magnitude() == "norme")
			return (0);
		else {
			$multi_l = $this->magnitude() * $rhs->magnitude();
			return ($this->dotProduct( $rhs ) / $multi_l );
		}
	}

	public function crossProduct( Vector $rhs ) {
		$cross = new Vector( array( 'dest' => new Vertex( array (
			'x' => $this->_y * $rhs->_z - $this->_z * $rhs->_y,
			'y' => $this->_z * $rhs->_x - $this->_x * $rhs->_z,
			'z' => $this->_x * $rhs->_y - $this->_y * $rhs->_x ) ) ) );
		return ( $cross );
	}

	public function __get( $attr )
	{
		if ( $attr == _x )
			return ( $this->getX() );
		else if ( $attr == _y )
			return ( $this->getY() );
		else if ( $attr == _z )
			return ( $this->getZ() );
		else if ( $attr == _w )
			return ( $this->getW() );
		else if ( $attr == _color )
			return ( $this->getColor() );
	}

	private function getX () { return $this->_x; }
	private function getY () { return $this->_y; }
	private function getZ () { return $this->_z; }
	private function getW () { return $this->_w; }
	private function getColor () { return $this->_color; }
}
?>
