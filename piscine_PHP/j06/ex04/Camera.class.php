<?php
require_once 'Vertex.class.php';
require_once 'Vector.class.php';
require_once 'Matrix.class.php';
require_once 'Camera.class.php';
class Camera {
	private $_origin;
	private $_originInverse;
	private $_orientation;
	private $_width;
	private $_height;
	private $_ratio;
	private $_fov;
	private $_near;
	private $_far;
	private $_tT;
	private $_tR;
	private $_tRxtT;
	private $_proj;
	private $_xc;
	private $_yc;
	private $_zc;
	private $_wc;
	private $_presetc;
	private $_anglec;
	static $verbose = false;

	public function __construct (array $kwargs) {
		if (isset($kwargs['ratio'])) {
			$this->_ratio = $kwargs['ratio'];
		}
		else if (isset($kwargs['width']) &&
			isset($kwargs['height'])) {
			$this->_width = $kwargs['width'];
			$this->_height = $kwargs['height'];
			$this->_ratio = $this->_width / $this->_height;
		}
		if (isset($kwargs['origin']) &&
			isset($kwargs['orientation']) &&
			isset($kwargs['fov']) &&
			isset($kwargs['near']) &&
			isset($kwargs['far'])) {
			$this->_xc = $kwargs['origin']->_x;
			$this->_yc = $kwargs['origin']->_y;
			$this->_zc = $kwargs['origin']->_z;
			$this->_wc = $kwargs['origin']->_w;
			$this->_origin = new Vertex(array(
				'x' => $this->_xc,
				'y' => $this->_yc,
				'z' => $this->_zc,
				'w' => $this->_wc));
			$this->_originInverse = new Vertex(array(
				'x' => $this->_xc * (-1),
				'y' => $this->_yc * (-1),
				'z' => $this->_zc * (-1),
				'w' => $this->_wc));

			$this->_anglec = $kwargs['orientation']->_angle;
			$test1 = new Matrix(array('preset' => Matrix::RX, 'angle' => $this->_anglec));
			$test2 = new Matrix(array('preset' => Matrix::RY, 'angle' => $this->_anglec));
			$test3 = new Matrix(array('preset' => Matrix::RZ, 'angle' => $this->_anglec));
			if ($test1 == $kwargs['orientation']) {
				$this->_orientation = new Matrix(array(
					'preset' => Matrix::RX,
					'angle' => $this->_anglec));

			}
			else if ($test2 == $kwargs['orientation']) {
				$this->_orientation = new Matrix(array(
					'preset' => Matrix::RY,
					'angle' => $this->_anglec));
			}
			else if ($test3 == $kwargs['orientation']) {
				$this->_orientation = new Matrix(array(
					'preset' => Matrix::RZ,
					'angle' => $this->_anglec));
			}
			
			$this->_fov = $kwargs['fov'];
			$this->_near = $kwargs['near'];
			$this->_far = $kwargs['far'];
			
			echo $this->_orientation;
			$this->_tT = new Matrix( array(
				'preset' => Matrix::TRANSLATION,
				'vtc' => $this->_originInverse ) );
			$this->_tR = Matrix::inverse($this->_orientation);
			$this->_tRxtT = $this->_tR->mult($this->_tT);
			$this->_proj = new Matrix( array( 'preset' => Matrix::PROJECTION,
				'fov' => $this->_fov,
				'ratio' => $this->_ratio,
				'near' => $this->_near,
				'far' => $this->_far ) );
		}
		if (self::$verbose)
			printf("Camera instance contructed\n");
	}

	public function __destruct () {
		if (self::$verbose)
			printf("Camera instance destructed\n");
	}

	public function __tostring () {
		$res = sprintf("Camera(\n+ Origine: $this->_origin\n+ tT:\n$this->_tT\n+ tR:\n$this->_tR\n+ tR->mult( tT ):\n$this->_tRxtT\n+ Proj:\n$this->_proj\n)");
		return ($res);
	}

	public function doc() {
		if ($str = file_get_contents('Camera.doc.txt'))
			echo $str;
		else
			echo "Error: .doc file doesn't exist.\n";
	}

	public function watchVertex( Vertex $worldVertex ) {
		$temp = $this->_tRtT->transformVertex( $worldVertex );
		$temp = $this->_proj->transformVertex( $temp );
		return ($temp );
	}
}
?>
