<?php
require_once 'Color.class.php';
require_once 'Vertex.class.php';
require_once 'Vector.class.php';
class Matrix {
	private $_preset;
	private $_scale;
	private $_angle;
	private $_inverse;
	private $_vtc;
	private $_fov;
	private $_ratio;
	private $_near;
	private $_far;
	private $_matrix;
	static $verbose = false;
	const IDENTITY = "IDENTITY";
	const SCALE = "SCALE";
	const RX = "Ox ROTATION";
	const RY = "Oy ROTATION";
	const RZ = "Oz ROTATION";
	const TRANSLATION = "TRANSLATION";
	const PROJECTION = "PROJECTION";

	public function __construct($matrix = null) {
		if (isset($matrix)) {
			echo $matrix['inverse'];
			$this->parse($matrix);
			$this->init_Matrix();
			if (self::$verbose) {
				if ($this->_preset == self::IDENTITY) {
					echo "Matrix " . $this->_preset . " instance constructed\n";
				} else {
					echo "Matrix " . $this->_preset .
						" preset instance constructed\n";
				}
			}
			$this->choose_func();
		}
	}

	private function parse($mtrx) {
		if (isset($mtrx['preset'])) {
			$this->_preset = $mtrx['preset'];
		}
		if (isset($mtrx['scale'])) {
			$this->_scale = $mtrx['scale'];
		}
		if (isset($mtrx['angle'])) {
			$this->_angle = $mtrx['angle'];
		}
		if (isset($mtrx['vtc'])) {
			$this->_vtc = $mtrx['vtc'];
		}
		if (isset($mtrx['fov'])) {
			$this->_fov = $mtrx['fov'];
		}
		if (isset($mtrx['ratio'])) {
			$this->_ratio = $mtrx['ratio'];
		}
		if (isset($mtrx['near'])) {
			$this->_near = $mtrx['near'];
		}
		if (isset($mtrx['far'])) {
			$this->_far = $mtrx['far'];
		}
	}
	private function choose_func() {
		switch ($this->_preset) {
		case (self::IDENTITY) :
			$this->identity(1);
			break;
		case (self::TRANSLATION) :
			$this->translation();
			break;
		case (self::SCALE) :
			$this->identity($this->_scale);
			break;
		case (self::RX) :
			$this->rotation_x();
			break;
		case (self::RY) :
			$this->rotation_y();
			break;
		case (self::RZ) :
			$this->rotation_z();
			break;
		case (self::PROJECTION) :
			$this->projection();
			break;
		}
	}

	private function init_Matrix()
	{
		for ($i = 0; $i < 16; $i++) {
			$this->_matrix[$i] = 0;
		}
	}
	public function mult(Matrix $rhs) {
		$tmp = array();
		for ($i = 0; $i < 16; $i += 4) {
			for ($j = 0; $j < 4; $j++) {
				$tmp[$i + $j] = 0;
				$tmp[$i + $j] += $this->_matrix[$i + 0] *
					$rhs->_matrix[$j + 0];
				$tmp[$i + $j] += $this->_matrix[$i + 1] *
					$rhs->_matrix[$j + 4];
				$tmp[$i + $j] += $this->_matrix[$i + 2] *
					$rhs->_matrix[$j + 8];
				$tmp[$i + $j] += $this->_matrix[$i + 3] *
					$rhs->_matrix[$j + 12];
			}
		}
		$clone = new Matrix();
		$clone->_matrix = $tmp;
		return $clone;
	}
	private function translation()
	{
		$this->identity(1);
		$this->_matrix[3] = $this->_vtc->_x;
		$this->_matrix[7] = $this->_vtc->_y;
		$this->_matrix[11] = $this->_vtc->_z;
	}
	public function inverse(Matrix $mtx)
	{
		$x1 = $mtx->_matrix[1];
		$x2 = $mtx->_matrix[2];
		$x3 = $mtx->_matrix[3];
		$mtx->_matrix[1] = $mtx->_matrix[4];
		$mtx->_matrix[2] = $mtx->_matrix[8];
		$mtx->_matrix[3] = $mtx->_matrix[12];
		$mtx->_matrix[4] = $x1;
		$mtx->_matrix[8] = $x2;
		$mtx->_matrix[12] = $x3;

		$x1 = $mtx->_matrix[6];
		$x2 = $mtx->_matrix[7];
		$mtx->_matrix[6] = $mtx->_matrix[9];
		$mtx->_matrix[7] = $mtx->_matrix[13];
		$mtx->_matrix[9] = $x1;
		$mtx->_matrix[13] = $x2;


		$x0 = $mtx->_matrix[11];
		$mtx->_matrix[11] = $mtx->_matrix[14];
		$mtx->_matrix[14] = $x0;
		return ($mtx);
	}
	private function rotation_x() {
		$this->identity(1);
		$this->_matrix[0] = 1;
		$this->_matrix[5] = cos($this->_angle);
		$this->_matrix[6] = -sin($this->_angle);
		$this->_matrix[9] = sin($this->_angle);
		$this->_matrix[10] = cos($this->_angle);
	}
	private function rotation_y() {
		$this->identity(1);
		$this->_matrix[0] = cos($this->_angle);
		$this->_matrix[2] = sin($this->_angle);
		$this->_matrix[5] = 1;
		$this->_matrix[8] = -sin($this->_angle);
		$this->_matrix[10] = cos($this->_angle);
	}
	private function rotation_z() {
		$this->identity(1);
		$this->_matrix[0] = cos($this->_angle);
		$this->_matrix[1] = -sin($this->_angle);
		$this->_matrix[4] = sin($this->_angle);
		$this->_matrix[5] = cos($this->_angle);
		$this->_matrix[10] = 1;
	}
	private function projection() {
		$this->identity(1);
		$this->_matrix[5] = 1 / tan(0.5 * deg2rad($this->_fov));
		$this->_matrix[0] = $this->_matrix[5] / $this->_ratio;
		$this->_matrix[10] = -1 * (-$this->_near - $this->_far) /
			($this->_near - $this->_far);
		$this->_matrix[14] = -1;
		$this->_matrix[11] = (2 * $this->_near * $this->_far) /
			($this->_near - $this->_far);
		$this->_matrix[15] = 0;
	}
	private function identity($scale)
	{
		$this->_matrix[0] = $scale;
		$this->_matrix[5] = $scale;
		$this->_matrix[10] = $scale;
		$this->_matrix[15] = 1;
	}
	public function transformVertex(Vertex $vtx) {
		$tmp = array();
		$tmp['x'] = ($vtx->_x * $this->_matrix[0]) +
			($vtx->_y * $this->_matrix[1]) + ($vtx->_z * $this->_matrix[2]) +
			($vtx->_w * $this->_matrix[3]);
		$tmp['y'] = ($vtx->_x * $this->_matrix[4]) +
			($vtx->_y * $this->_matrix[5]) + ($vtx->_z * $this->_matrix[6]) +
			($vtx->_w * $this->_matrix[7]);
		$tmp['z'] = ($vtx->_x * $this->_matrix[8]) +
			($vtx->_y * $this->_matrix[9]) + ($vtx->_z * $this->_matrix[10]) +
			($vtx->_w * $this->_matrix[11]);
		$tmp['w'] = ($vtx->_x * $this->_matrix[11]) +
			($vtx->_y * $this->_matrix[13]) + ($vtx->_z * $this->_matrix[14]) +
			($vtx->_w * $this->_matrix[15]);
		$tmp['color'] = $vtx->_color;
		$vertex = new Vertex($tmp);
		return $vertex;
	}
	public function __destruct() {
		if (self::$verbose) {
			print("Matrix instance destructed\n");
		}
	}
	public function __toString()
	{
		$res = "M | vtcX | vtcY | vtcZ | vtxO\n";
		$res .= "-----------------------------\n";
		$res .= "x | %0.2f | %0.2f | %0.2f | %0.2f\n";
		$res .= "y | %0.2f | %0.2f | %0.2f | %0.2f\n";
		$res .= "z | %0.2f | %0.2f | %0.2f | %0.2f\n";
		$res .= "w | %0.2f | %0.2f | %0.2f | %0.2f";
		return (vsprintf($res, array(
			$this->_matrix[0], $this->_matrix[1], $this->_matrix[2], $this->_matrix[3],
			$this->_matrix[4], $this->_matrix[5], $this->_matrix[6], $this->_matrix[7],
			$this->_matrix[8], $this->_matrix[9], $this->_matrix[10], $this->_matrix[11],
			$this->_matrix[12], $this->_matrix[13], $this->_matrix[14], $this->_matrix[15])));
	}
	public function doc() {
		if ($str = file_get_contents('Matrix.doc.txt')) {
			echo "$str";
		}
		else {
			echo "Error: .doc file doesn't exist.\n";
		}
	}
	public function __get( $attr )
	{
		if ( $attr == _present )
			return ( $this->getPresent() );
		else if ( $attr == _angle )
			return ( $this->getAngle() );
	}

	private function getPresent () { return $this->_present; }
	private function getAngle () { return $this->_angle; }
}
?>
