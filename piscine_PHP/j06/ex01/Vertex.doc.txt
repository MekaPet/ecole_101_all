<- Vertex ----------------------------------------------------------------------
La class Vertex donne les coordonees d'un point dans l'espace ainsi qu'une couleur a ce point.

Une instance peut-etre construite sans couleur et sans coordonees homogenes:
new Vertex( array( 'x' => 0.0, 'y' => 0.0, 'z' => 0.0 ) );

Une instance peut-etre construite sans couleur:
new Vertex( array( 'x' => 0.0, 'y' => 0.0, 'z' => 0.0, 'w' => 0.0 ) );

Une instance peut-etre construite sans coordonees homogenes:
$blue  = new Color( array( 'red' =>   0, 'green' =>   0, 'blue' => 255 ) );
new Vertex( array( 'x' => 0.0, 'y' => 0.0, 'z' => 1.0, 'color' => $blue  ) );

Une instance peut-etre construite avec tout les parametres:
$blue  = new Color( array( 'red' =>   0, 'green' =>   0, 'blue' => 255 ) );
new Vertex( array( 'x' => 0.0, 'y' => 0.0, 'z' => 1.0, 'w' => 0.0, 'color' => $blue  ) );

x correspond a son abscisse

y correspond a son ordonnee

z correspond a sa profondeur

w correspond a la coordonnee homogenes

color correspond a la couleur
---------------------------------------------------------------------- Vertex ->
