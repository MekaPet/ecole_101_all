<?php
	require_once('Color.class.php');
class Vertex {
	private $_x;
	private $_y;
	private $_z;
	private $_w = 1.0;
	private $_color;
	static $verbose = false;

	public function __construct ( array $kwargs ) {
		if (
			isset( $kwargs['x'] ) &&
			isset( $kwargs['y'] ) &&
			isset( $kwargs['z'] ) )
		{
			$this->_x = $kwargs['x'];
			$this->_y = $kwargs['y'];
			$this->_z = $kwargs['z'];
		}
		if ( isset( $kwargs['w'] ) )
			$this->_w = $kwargs['w'];
		if ( isset( $kwargs['color'] ) )
			$this->_color = $kwargs['color'];
		else {
			$this->_color = new Color ( array(
				'red' => 255,
				'green' => 255,
				'blue' => 255 ) );
		}
		if ( self::$verbose )
			printf( $this . " contructed\n" );
	}

	public function __destruct () {
		if ( self::$verbose )
			printf( $this . " destructed\n" );
	}

	public function __tostring() {
		if ( self::$verbose ) {
			$ret = sprintf( "Vertex( x: %.2f, y: %.2f, z:%.2f, w:%.2f, $this->_color )", $this->_x, $this->_y, $this->_z, $this->_w );
			return $ret;
		}
		else {
			$ret = sprintf( "Vertex( x: %.2f, y: %.2f, z:%.2f, w:%.2f )", $this->_x, $this->_y, $this->_z, $this->_w );
			return $ret;
		}
	}

	public function doc() {
		if ( $str = file_get_contents( 'Vertex.doc.txt' ) ) {
			echo $str;
		}
		else {
			echo "Error: .doc file doesn't exist.\n";
		}
	}

	public function __get( $attr )
	{
		if ( $attr == _x )
			return ( $this->getX() );
		else if ( $attr == _y )
			return ( $this->getY() );
		else if ( $attr == _z )
			return ( $this->getZ() );
		else if ( $attr == _w )
			return ( $this->getW() );
		else if ( $attr == _color )
			return ( $this->getColor() );
	}

	public function __set( $attr, $value )
	{
		if ( $attr == _x )
			return ( $this->setX( $value ) );
		else if ( $attr == _y )
			return ( $this->setY( $value ) );
		else if ( $attr == _z )
			return ( $this->setZ( $value ) );
		else if ( $attr == _w )
			return ( $this->setW( $value ) );
		else if ( $attr == _color )
			return ( $this->setColor( $value ) );
	}

	private function getX () { return $this->_x; }
	private function getY () { return $this->_y; }
	private function getZ () { return $this->_z; }
	private function getW () { return $this->_w; }
	private function getColor () { return $this->_color; }

	private function setX ( $value ) {
		$this->_x = $value;
	}
	private function setY ( $value ) {
		$this->_y = $value;
	}
	private function setZ ( $value ) {
		$this->_z = $value;
	}
	private function setW ( $value ) {
		$this->_x = $value;
	}
	private function setColor ( $value ) {
		$this->_x = $value;
	}
}
?>
