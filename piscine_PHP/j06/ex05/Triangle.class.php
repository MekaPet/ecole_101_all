<?php
require_once('Vertex.class.php');
class Triangle {
	private $_A;
	private $_B;
	private $_C;
	private $_xc;
	private $_yc;
	private $_zc;
	private $_wc;
	static $verbose = false;

	public function __construct (array $kwargs) {
		if (isset($kwargs['A']) &&
			isset($kwargs['B']) &&
			isset($kwargs['C'])) {
			$this->_xc = $kwargs['A']->_x;
			$this->_yc = $kwargs['A']->_y;
			$this->_zc = $kwargs['A']->_z;
			$this->_wc = $kwargs['A']->_w;
			$this->_A = new Vertex(array(
				'x' => $this->_xc,
				'y' => $this->_yc,
				'z' => $this->_zc,
				'w' => $this->_wc));
			$this->_xc = $kwargs['B']->_x;
			$this->_yc = $kwargs['B']->_y;
			$this->_zc = $kwargs['B']->_z;
			$this->_wc = $kwargs['B']->_w;
			$this->_B = new Vertex(array(
				'x' => $this->_xc,
				'y' => $this->_yc,
				'z' => $this->_zc,
				'w' => $this->_wc));
			$this->_xc = $kwargs['C']->_x;
			$this->_yc = $kwargs['C']->_y;
			$this->_zc = $kwargs['C']->_z;
			$this->_wc = $kwargs['C']->_w;
			$this->_C = new Vertex(array(
				'x' => $this->_xc,
				'y' => $this->_yc,
				'z' => $this->_zc,
				'w' => $this->_wc));
			if (self::$verbose)
				print("Triangle instance constructed\n");
		}
	}

	public function __destruct () {
		if (self::$verbose)
			print("Triangle instance destructed\n");
	}
	public function doc() {
		if ( $str = file_get_contents( 'Triangle.doc.txt' ) ) {
			echo $str;
		}
		else {
			echo "Error: .doc file doesn't exist.\n";
		}
	}
}
?>
