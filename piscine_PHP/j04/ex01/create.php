<?php
if ($_POST && isset($_POST['login']) && isset($_POST['passwd']) && isset($_POST['submit']) && $_POST['submit'] == "OK")
{
	if (!file_exists('../htdocs/'))
		mkdir('../htdocs/');
	if (!file_exists('../htdocs/private'))
		mkdir('../htdocs/private');
	if (file_exists('../htdocs/private/passwd'))
	{
		$content_passwd = file_get_contents('../htdocs/private/passwd');
		$db_utilisateur = unserialize($content_passwd);
	}
	else
		$db_utilisateur = "";
	$existant = 0;
	if ($db_utilisateur)
	{
		foreach ($db_utilisateur as $key => $utilisateur)
		{
			if ($utilisateur['login'] === $_POST['login'])
				$existant = 1;
		}
	}
	if ($existant)
		echo "ERROR\n";
	else
	{
		$db_utilisateur[] = array('login' => $_POST['login'], 'passwd' => hash('whirlpool', $_POST['passwd']));
		file_put_contents('../htdocs/private/passwd', serialize($db_utilisateur));
		echo "OK\n";
	}
}
else
	echo "ERROR\n";
?>
