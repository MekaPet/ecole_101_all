<?php
if ($_POST && isset($_POST['login']) && isset($_POST['oldpw']) && isset($_POST['newpw']) && isset($_POST['submit']) && $_POST['submit'] == "OK" && file_exists('../htdocs/private/passwd'))
{
	$content_passwd = file_get_contents('../htdocs/private/passwd');
	$db_utilisateur = unserialize($content_passwd);
	$existant = 0;
	foreach ($db_utilisateur as $key => $value)
	{
		if ($value['login'] === $_POST['login'] && $value['passwd'] === hash('whirlpool', $_POST['oldpw']))
		{
			$existant = 1;
			$db_utilisateur[$key]['passwd'] = hash('whirlpool', $_POST['newpw']);
		}
	}
	if ($existant)
	{
		file_put_contents('../htdocs/private/passwd', serialize($db_utilisateur));
		echo "OK\n";
	}
	else
		echo "ERROR\n";
}
else
	echo "ERROR\n";
?>
