<?php
function auth($login, $passwd)
{
	if (!$login || !$passwd)
		return false;
	$content_passwd = file_get_contents('../htdocs/private/passwd');
	$db_utilisateur = unserialize($content_passwd);
	foreach ($db_utilisateur as $key => $utilisateur)
	{
		if ($utilisateur['login'] === $login && $utilisateur['passwd'] === hash('whirlpool', $passwd))
			return true;
	}
return false;
}
?>
