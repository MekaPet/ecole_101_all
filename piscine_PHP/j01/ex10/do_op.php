#!/usr/bin/php
<?php
if ($argc == 1 || $argc > 4)
{
	echo "Incorrect Parameters\n";
	exit;
}
$res = array_merge(preg_split('/\s+/', $argv[1], -1, PREG_SPLIT_NO_EMPTY), preg_split('/\s+/', $argv[2], -1, PREG_SPLIT_NO_EMPTY));
$res = array_merge($res, preg_split('/\s+/', $argv[3], -1, PREG_SPLIT_NO_EMPTY));
if ($res[1] == "+")
	echo $res[0] + $res[2]."\n";
if ($res[1] == "-")
	echo $res[0] - $res[2]."\n";
if ($res[1] == "*")
	echo $res[0] * $res[2]."\n";
if ($res[1] == "/")
	echo $res[0] / $res[2]."\n";
if ($res[1] == "%")
	echo $res[0] % $res[2]."\n";
?>
