#!/usr/bin/php
<?php
if ($argc < 2)
	exit;
$input = preg_split('/\s+/', $argv[1], -1, PREG_SPLIT_NO_EMPTY);
$i = 0;
while ($input[$i])
{
	if ($i == 0)
		echo "$input[0]";
	if ($i > 0)
	{
		$in = $input[$i];
		echo " $in";
	}
	$i++;
}
echo "\n";
?>
