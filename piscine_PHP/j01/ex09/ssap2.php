#!/usr/bin/php
<?PHP
function cut_av($str)
{
	$tab = preg_split('/\s+/', $str, -1, PREG_SPLIT_NO_EMPTY);
	return ($tab);
}
if ($argc == 1)
	exit;
$i = 1;
$j = 0;
$final_res = array();
$tmp_tab = array();
while ($argv[$i])
{
	$tmp_tab = cut_av($argv[$i]);
	$final_res = array_merge($final_res, $tmp_tab);
	$i++;
}
$i = 0;
$tab_num = array();
$tab_string = array();
$tab_other = array();
$test = array();
while ($final_res[$i])
{
	$test = preg_split('/\s+/', $final_res[$i], -1, PREG_SPLIT_NO_EMPTY);
	if (is_numeric($final_res[$i][0]))
		$tab_num = array_merge($tab_num, $test);
	if (ctype_alpha($final_res[$i][0]))
		$tab_string = array_merge($tab_string, $test);
	if (!ctype_alpha($final_res[$i][0]) && !is_numeric($final_res[$i][0]))
		$tab_other = array_merge($tab_other, $test);
	$i++;
}
sort($tab_num, SORT_STRING);
natcasesort($tab_string);
sort($tab_other);
function maj($a)
{
	$model = " abcdefghijklmnopqrstuvwxyz";
	$maj = " ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	$i = 0;
	while ($maj[$i] && $maj[$i] != $a)
	{
		$i++;
		if ($a == $maj[$i])
			$a = $model[$i];
	}
	return ($a);
}
function countmod($a)
{
	$model = "abcdefghijklmnopqrstuvwxyz0123456789 !\"#$%&'()*+,-./:;<=>?@[\]^_`{|}~";
	$i = 0;
	while ($model[$i] != "\0" && $model[$i] != $a)
		$i++;
	$i--;
	return ($i);
}

function cmp($a, $b)
{
	$nb_a = 0;
	$nb_b = 0;
	$i = 0;
	while ($a[$i] || $b[$i])
	{
		$nb_a = $nb_a + countmod(maj($a[$i]));
		$nb_b = $nb_b + countmod(maj($b[$i]));
		if ($nb_a < $nb_b)
			return (-1);
		if ($nb_a > $nb_b)
			return (1);
		$i++;
	}
	if ($nb_a == $nb_b)
		return (0);
}

usort($tab_string, "cmp");
usort($tab_num, "cmp");
usort($tab_other, "cmp");
$res = array_merge($tab_string, $tab_num);
$res = array_merge($res, $tab_other);
$i = 0;
while ($res[$i])
{
	echo $res[$i]."\n";
	$i++;
}
?>
