#!/usr/bin/php
<?php
while (1):
	echo "Entrez un nombre: ";
	$stdin = fopen('php://stdin', 'r');
	$input = fgets($stdin);
	if ($input == NULL)
	{
		echo "^D\n";
		exit;
	}
	$input = explode("\n", $input);
	$input = $input[0];
	fclose($stdin);
	if (is_numeric($input))
	{
		if ($input % 2 == 0)
			echo "Le chiffre $input est Pair\n";
		else
			echo "Le chiffre $input est Impair\n";
	}
	else
		echo "'$input' n'est pas un chiffre\n";
endwhile
?>
