#!/usr/bin/php
<?php
$i = 1;
if ($argc == 1)
{
	exit;
}
while ($i != $argc)
{
	$input = preg_split('/\s+/', $argv[$i], -1, PREG_SPLIT_NO_EMPTY);
	if ($i == 1)
		$res = $input;
	else
		$res = array_merge($res, $input);
	$i++;
}
sort($res);
$i = 0;
while ($res[$i])
{
	print("$res[$i]\n");
	$i++;
}
?>
